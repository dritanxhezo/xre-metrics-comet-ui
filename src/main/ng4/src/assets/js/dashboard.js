/* ===========================================================================
 * Copyright (c) 2016 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Oleksii Shapovalov
 * Created: 04/22/16
 */
'use strict';

window.onload = function() {
    checkHideParams();
};

var checkHideParams = function() {
    if (window.location.hash.indexOf("hidePageHeader=true") != -1) {
        $(".page-header").hide();
    }
    if (window.location.hash.indexOf("hideMenu=true") != -1) {
        $("#menuZone").hide();
    }
};

var Dashboard = function(dashboardId, containerStyle) {  // dataSource, 
    checkHideParams();
    if (containerStyle == undefined) {
        containerStyle = "col-md-2";
    }

    this.containerStyle = containerStyle;
//    var currentUrl = window.location.hash;
//    if (currentUrl.indexOf("receiver=") != -1) {
//        currentUrl.replace(new RegExp("receiver=(.*)&?"), function(all, first) {
//            dataSource = dataSource + "?receiverId=" + first;
//        });
//    }
//
//    this.dataSource = dataSource;
    this.dashboard = d3.select('#' + dashboardId)
        .append("div")
        .classed("container-fluid", true)
        ;

    this.services = [];

    this.animationDivider = 12;
    this.animationTime = 3000;

    this.maxDataLimitForServices = 0;
    this.maxDataLimitForSubservice  = 0;


};

Dashboard.prototype.processRows = function(rows) {
    //--uppercase

    for (var i = 0; i < rows.length; i++) {
        rows[i].service = rows[i].service.replace(new RegExp("(^.)"), function(d) {return d.toUpperCase();})
    }
    //--maxDataLimitForServices
    var services = this.getServices();
    var max = 0;
    for (var i = 0; i < services.length; i++) {
        var service = services[i];
        max = Math.max(getServiceTotalCount(rows, service), max);
    }

    if (max > this.maxDataLimitForServices) {
        this.maxDataLimitForServices = max * 1.2;
    }
    //--
};

Dashboard.prototype.getMaxDataLimitForSubservice = function() {
    var serviceName = this.expandedServiceName;
    var subservices = getSubservicesForService(this.rows, serviceName);
    var max = 0;
    for (var i = 0; i < subservices.length; i++) {
        var subservice = subservices[i];
        max = Math.max(max, getCountForSubservice(this.rows, serviceName, subservice));
    }

    if (max > this.maxDataLimitForSubservice) {
        this.maxDataLimitForSubservice = max * 1.2;
    }
    return this.maxDataLimitForSubservice;

};

Dashboard.prototype.drawSubserviceTable = function(serviceName) {
    var _this = this;
    var targetElement = {};
    this.dashboard
        .selectAll("div.dashboard-cell-container")
        .each(function() {
            var currentElement = d3.select(this);
            if (currentElement.select("div.dashboard-cell div.dashboard-cell-label").text() == serviceName) {
                targetElement = currentElement;
            }
        });

    targetElement
        .select(".dashboard-cell")
        .classed("dashboard-cell-selected", true);
    targetElement.attr("expanded", true)
    ;

    var borderOutside = 64;
    _this.selectedServiceName = serviceName;
    _this.expandedCell = targetElement.append("div")
        .classed("dashboard-expand-cell", true)
        .style("z-index", "10000")
        .style("margin-left", function() {
            var elementLeft = this.getBoundingClientRect().left;
            var dashboardLeft = _this.dashboard.node().getBoundingClientRect().left;
            return -1 * (elementLeft - dashboardLeft - borderOutside / 2) + "px";
        })
        .style("width", function(){
            _this.dashboardWidth = _this.dashboard.node().getBoundingClientRect().width;
            return _this.dashboardWidth - borderOutside + "px";
        })
    ;
    
    this.expandedCell.append("div")
        .classed("expanded-service-label", true)
        .text(serviceName);
    

    this.subserviceTable = this.expandedCell.append("table")
        .classed("subservice-table", true)
    ;
    this.subserviceThead = this.subserviceTable.append("thead");
    this.subserviceTbody = this.subserviceTable.append("tbody");

    this.subserviceThead.append("th")
        .classed("cell-subservicename", true)
        .append("div")
        .classed("table-cell", true)
        .text("Subservice")
    ;
    this.subserviceThead.append("th")
        .classed("cell-totalcount", true)
        .append("div")
        .classed("table-cell", true)
        .text("Total count")
    ;

    this.subserviceList = getSubservicesForService(this.rows, this.expandedServiceName);
    this.subserviceTableRows = this.subserviceTbody.selectAll("tr")
        .data(this.subserviceList)
        .enter().append("tr")
        ;

    this.subserviceTableCells = this.subserviceTableRows.selectAll("td")
        .data(function(data) {
            return [data, ""]
        })
        .enter()
        .append("td")
        .classed("cell-subservicename", function(data, i) {
            return i % 2 == 0;
        })
        .classed("cell-totalcount", function(data, i) {
            return i % 2 == 1;
        })
        .append("div")
        .classed("table-cell", true)
        ;

    this.subserviceTableRows.selectAll(".cell-subservicename div")
        .classed("subservice-label-name", true);

    this.subserviceTableRows.selectAll(".cell-subservicename")
        .append("div")
        .classed("cell-successrate", true);
    
    this.updateSubserviceTable(this.rows);

};

Dashboard.prototype.updateSubserviceTable = function(rows) {
    var _this = this;
    if (this.expandedCell != undefined) {

        if (this.subserviceTableForceRedraw) {
            this.subserviceTableForceRedraw = false;
            this.collapseAllCells();
            this.drawSubserviceTable(this.expandedServiceName);
        }

        this.subserviceTbody.selectAll("tr")
            .data(this.subserviceList)
            .selectAll(".cell-subservicename div.subservice-label-name")
            .text(function (data) {
                return data;
            });

        this.subserviceTbody.selectAll("tr")
            .data(this.subserviceList)
            .selectAll(".cell-subservicename div.cell-successrate")
            .html(function(data) {
                d3.select(this)
                    .classed("colored-green colored-yellow colored-red", false);
                
                var successRateValue = getSuccessRateForSubservice(rows, _this.expandedServiceName, data);
                var countValue = getCountForSubservice(rows, _this.expandedServiceName, data);
                
                if (countValue == 0) {
                    var labelClass = "colored-black-normal";
                    return "-%";
                }
                successRateValue = successRateValue / 100;

                if (countValue != 0) {
                    if (successRateValue > 95) {
                        labelClass = "colored-green";
                    } else if (successRateValue > 75) {
                        labelClass = "colored-yellow";
                    } else {
                        labelClass = "colored-red";
                    }
                }
                
                d3.select(this)
                    .classed("colored-green colored-yellow colored-red", false)
                    .classed(labelClass, true);

                return successRateValue.toFixed(2) + "%";
                
            });


        this.subserviceTbody.selectAll("tr")
            .data(this.subserviceList)
            .selectAll(".cell-totalcount div")
            .data(function(data) {
                return [data];
            })
            .classed("totalcount-chart", true)
            .transition()
            .duration(_this.animationTime)
            .style("width", function(data) {
                var totalCount = getCountForSubservice(rows, _this.expandedServiceName, data);
                var currentCell = d3.select(this.parentNode);
                var cellWidth = parseInt(currentCell.style("width")) - 20;

                var serviceTotalScaleFunction = d3.scale.linear()
                    .domain([0, _this.getMaxDataLimitForSubservice()])
                    .range([0, cellWidth]);

                return serviceTotalScaleFunction(totalCount) + "px";
            })
            .tween("text", function(data) {
                if (_this.previousRows == undefined) {
                    var currentValue = 0;
                } else {
                    currentValue = getCountForSubservice(_this.previousRows, _this.expandedServiceName, data);
                }

                var nextValue = getCountForSubservice(rows, _this.expandedServiceName, data);
                var interpolator = d3.interpolateRound(currentValue, nextValue);
                var dividerCounter = 0;
                var dividerHolder;
                return function(t) {
                    if (dividerCounter % _this.animationDivider == 0) {
                        dividerHolder = interpolator(t).toLocaleString("en");
                    }
                    dividerCounter++;
                    this.textContent = dividerHolder;
                };
            })
    }

};


Dashboard.prototype.collapseAllCells = function() {
    this.expandedCell = undefined;
    this.maxDataLimitForSubservice  = 0;
    this.dashboard.selectAll("div.dashboard-cell-container")
        .each(function() {
        var targetElement = d3.select(this);
        var expanded = targetElement.attr("expanded");
        if (expanded == "true") {
            targetElement.select(".dashboard-cell").classed("dashboard-cell-selected", false);
            targetElement.attr("expanded", false);

            targetElement.selectAll(".dashboard-expand-cell").remove();
        }
    });
};

Dashboard.prototype.getDataAndDraw = function() {
    var _this = this;
    d3.json(this.dataSource, function (error, response) {
        if (error == null) {
            var rows = response.microserviceDataModelList;
            if (_this.rows != undefined) {
                _this.previousRows = _this.rows;
            }
            _this.rows = rows;
            _this.touchedServices = d3.set(response.touchedServices);
            _this.processRows(rows);
            _this.drawDashboard(rows);
            _this.updateSubserviceTable(rows);
        } else {
            console.log("Error getting data from server " + error);
        }
    });
};

var getServiceTotalCount  = function(rows, serviceName) {
    return rows.filter(function(row) {
            return row.service == serviceName;
        })
        .reduce(function(sum, row) {
            return sum + row.count;
        }, 0);
};

var getServiceSuccessRate  = function(rows, serviceName) {
    var total =  rows.filter(function(row) {
            return row.service == serviceName;
        })
        .reduce(function(sum, row) {
            return sum + row.count;
        }, 0);

    var success = rows.filter(function(row) {
            return row.service == serviceName;
        })
        .reduce(function(sum, row) {
            return sum + row.successCount;
        }, 0);

    if (total == 0) {
        return 0;
    }
    
    return (success * 10000 / total);
};

Dashboard.prototype.getServices = function() {
    var services = this.rows.map(function (rowElement) {
        return rowElement.service;
    });
    services = d3.merge([services, this.services]);
    services = d3.set(services).values();
    services.sort();
    this.services = services;
    return services;
};

var getSubservicesForService = function(rows, serviceName) {
    var subservices = rows.filter(function(row) {
        return row.service == serviceName;
    })
        .map(function(row) {
            return row.subService;
        });
    subservices = d3.set(subservices).values();
    subservices.sort();
    return subservices;
};

var getCountForSubservice = function(rows, serviceName, subserviceName) {
    return rows.filter(function(row) {
        return row.service == serviceName && row.subService == subserviceName;
    })
        .reduce(function(sum, row) {
        return sum + row.count;
    }, 0);
};

var getSuccessCountForSubservice = function(rows, serviceName, subserviceName) {
    return rows.filter(function(row) {
            return row.service == serviceName && row.subService == subserviceName;
        })
        .reduce(function(sum, row) {
            return sum + row.successCount;
        }, 0);
};

var getSuccessRateForSubservice = function(rows, serviceName, subserviceName) {
    var totalCount = getCountForSubservice(rows, serviceName, subserviceName);
    var successCount = getSuccessCountForSubservice(rows, serviceName, subserviceName);
    if (totalCount != 0) {
        return (successCount * 100 * 100 / (totalCount));
    } else {
        return 0;
    }

};

Dashboard.prototype.updateDashboardData = function(rows) {
    var _this = this;

    this.dashboard.selectAll("div.dashboard-cell-container div.dashboard-cell div.dashboard-cell-label")
        .data(this.getServices())
        .text(function (data) {
            return data;
        });
    
    this.dashboard.selectAll("div.dashboard-cell-container div.dashboard-cell")
        .data(this.getServices())
        .filter(function(serviceName) {
            return _this.touchedServices.has(serviceName.toLowerCase());
        })
        .transition()
        .duration(this.animationTime / 2)
        .style("background-color", "pink")
        .transition()
        .duration(this.animationTime / 2)
        .style("background-color", "white");

    this.dashboard.selectAll("div.dashboard-cell-container div.dashboard-cell div.dashboard-cell-total-count")
        .data(this.getServices())
        .transition()
        .duration(this.animationTime)
        .style("width", function (data) {
            var totalCount = getServiceTotalCount(rows, data);
            var currentCell = d3.select(this.parentNode);
            var cellWidth = parseInt(currentCell.style("width")) - 40;

            var serviceTotalScaleFunction = d3.scale.linear()
                .domain([0, _this.maxDataLimitForServices])
                .range([0, cellWidth]);

            return serviceTotalScaleFunction(totalCount) + "px";
        })
        .tween("text", function(data) {
            if (_this.previousRows == undefined) {
                var currentValue = 0;
            } else {
                currentValue = getServiceTotalCount(_this.previousRows, data);
            }
            var nextValue = getServiceTotalCount(rows, data);
            var interpolator = d3.interpolateRound(currentValue, nextValue);
            var dividerCounter = 0;
            var dividerHolder;
            return function(t) {
                if (dividerCounter % _this.animationDivider == 0) {
                    dividerHolder = interpolator(t).toLocaleString("en");
                }
                dividerCounter++;
                this.textContent = dividerHolder;
            };
        })
    ;

    this.dashboard.selectAll("div.dashboard-cell-container div.dashboard-cell div.dashboard-cell-dynamic")
        .data(this.getServices())
        .html(function(data) {
            d3.select(this)
                .classed("colored-green colored-yellow colored-red", false);

            var successRateValue = getServiceSuccessRate(rows, data);
            var countValue = getServiceTotalCount(rows, data);

            if (countValue == 0) {
                var labelClass = "colored-black-normal";
                return "-%";
            }
            successRateValue = successRateValue / 100;

            if (countValue != 0) {
                if (successRateValue > 95) {
                    labelClass = "colored-green";
                } else if (successRateValue > 75) {
                    labelClass = "colored-yellow";
                } else {
                    labelClass = "colored-red";
                }
            }

            d3.select(this)
                .classed("colored-green colored-yellow colored-red", false)
                .classed(labelClass, true);

            return successRateValue.toFixed(2) + "%";
            
        });
};

Dashboard.prototype.clearDashboard = function() {
    var _this = this;

    this.dashboard.selectAll("div.dashboard-cell-container").remove();
}

Dashboard.prototype.drawDashboard = function(rows) {
    var services = this.getServices();
    var _this = this;

    this.dashboard.selectAll("div.dashboard-cell-container")
        .data(services)
        .enter().append("div")
        .classed("dashboard-cell-container", true)
        .classed(this.containerStyle, true)
        .attr("expanded", false)
        .call(function(dashboardCellContainer) {
            var elementArray = dashboardCellContainer[0];
            _this.subserviceTableForceRedraw = false;
            for (var i = 0; i < elementArray.length; i++) {
                if (elementArray[i] != null) {
                    _this.subserviceTableForceRedraw = true;
                    break;
                }
            }
            var dashboardCell = dashboardCellContainer.append("div")
                .classed("dashboard-cell", true)
            ;

            dashboardCell.append("div")
                .classed("dashboard-cell-label", true)
            ;

            var dynamicCell = dashboardCell.append("div")
                .classed("dashboard-cell-dynamic", true)
            ;

            dashboardCell.append("div")
                .classed("dashboard-cell-total-count", true)
                .style("width", "0px")
            ;

            _this.setEventListeners(dashboardCell, dashboardCellContainer);

        })
        ;

    this.updateDashboardData(rows);
};

Dashboard.prototype.setEventListeners = function(dashboardCell, dashboardCellContainer) {

    var _this = this;

    dashboardCell
        .on("mouseenter", function(data) {
            d3.select(this)
                .classed("dashboard-cell-highlighted", true);
        });

    dashboardCell
        .on("mouseleave", function(data) {
            d3.select(this)
                .classed("dashboard-cell-highlighted", false);
        });

    dashboardCellContainer
        .on("click", function(data) {
            var targetElement = d3.select(this);
            _this.expandedServiceName = targetElement.select("div.dashboard-cell div.dashboard-cell-label")
                .text();
            var expanded = targetElement.attr("expanded");
            if (expanded == "true") {
                _this.collapseAllCells();
            } else {
                _this.collapseAllCells();

                var serviceName = targetElement
                    .select("div.dashboard-cell div.dashboard-cell-label")
                    .text();

                _this.drawSubserviceTable(serviceName);
            }

        });
};

Dashboard.prototype.start = function(dataSource) {
	var _this = this;
	
    var currentUrl = window.location.hash;
    if (currentUrl.indexOf("receiver=") != -1) {
        currentUrl.replace(new RegExp("receiver=(.*)&?"), function(all, first) {
            dataSource = dataSource + "?receiverId=" + first;
        });
    }

    _this.dataSource = dataSource;	
	_this.getDataAndDraw();
    _this.refreshSchedule = setInterval(function() {_this.getDataAndDraw();}, 11000);
};

Dashboard.prototype.stop = function() {
    var _this = this;
    clearInterval(_this.refreshSchedule);
    _this.clearDashboard();
};
