export class KeyValue {
  itemValue: string;  // itemLabel, itemValue
  itemLabel: string;
  isChecked: boolean;
  isIndeterminate: boolean;

  constructor(itemValue: string, itemLabel: string, isChecked: boolean) {
    this.itemValue = itemValue;
    this.itemLabel = itemLabel;
    this.isChecked = isChecked;
  }
}
