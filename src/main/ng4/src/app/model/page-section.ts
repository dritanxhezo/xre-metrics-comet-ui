export class PageSection {
  id: string;
  dataType: string;
  header: string;
  description: string;
  data: any;
  width: number;
  height: number;
  order: number;
}
