import { Page } from './page';
export class Report {
  id: string;
  pages: Array<Page>;
}
