import { KeyValue } from './key-value';
export class SettingsResponse {
id: string;
friendlyName: string;
items: Array<KeyValue>; // left is machine value, right is human-readable  itemLabel, itemValue
selectedValue: string;
uiGroup: string;
}
