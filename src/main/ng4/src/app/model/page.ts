export class Page {
    title: string;
    id: string;
    chartIds: Array<string>;
    path: string;
    layout2charts: boolean;
    pureChart: boolean;
    deeplinkableCharts: boolean;
    roleRequired: Array<string>;

//  constructor(name: string) {
//    this.name = name;
//  }
}
