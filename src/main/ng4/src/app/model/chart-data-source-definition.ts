export class DataSourceDefinition {
  axisName: string;
  axisIndex: number;
  ticks: Array<Number>;
  dataSource: string;
  populationsDataSource: string;
}
