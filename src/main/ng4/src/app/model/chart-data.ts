import { ChartDataDef } from './chart-data-def';

export class ChartData {
  legend: Array<string>;
  data: Array<ChartDataDef>;
  timeZone: string;

  constructor(legend: Array<string>, data: Array<ChartDataDef>, timeZone: string) {
    this.legend = legend;
    this.data = data;
    this.timeZone = timeZone;
  }

}
