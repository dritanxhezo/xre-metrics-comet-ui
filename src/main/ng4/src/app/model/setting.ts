import { KeyValue } from './key-value';
export class Setting {
  id: string;
  type: string;
  dataSource: string;
  name: string;
  friendlyName: string;
  defaultValue: string;
  selectedValue: string;
  selectedLabel: string;
  uiGroup: string;
  valuesList: Array<KeyValue>;
  additionalItems: Array<KeyValue>;
  multiSelect: boolean;
}
