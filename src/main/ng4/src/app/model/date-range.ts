export class DateRange {
  period: number;
  min: Date;
  max: Date;

  constructor (period: number, min: Date, max: Date) {
    this.period = period;
    this.min = min;
    this.max = max;
  }
}
