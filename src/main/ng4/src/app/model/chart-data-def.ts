export class ChartDataDef {
  name: string;
  values: Array<number>;

  constructor(name: string, values: Array<number>) {
    this.name = name;
    this.values = values;
  }
}
