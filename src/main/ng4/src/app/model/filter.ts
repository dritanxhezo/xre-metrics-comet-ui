export class Filter {
  field: string;
  value: string;
  operation = 'equal';

  constructor (field: string, value: string) {
    this.field = field;
    this.value = value;
  }

  toString(): string {
      return this.field + '=' + this.value;
  }
}
