import { TableColumnDefinition } from './table-column-definition';
export class TableDefinition {
  columns: Array<TableColumnDefinition>;
  dataSource: string;
  tableFilter: string;
  sortedField: string;
  sortedOrder: string;
}
