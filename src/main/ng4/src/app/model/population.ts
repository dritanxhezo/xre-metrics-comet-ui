import { Filter } from './filter';
export class Population {
  name: string;
  filters: Filter[];
  params: Map<string, string>;
}
