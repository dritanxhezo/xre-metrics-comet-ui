import { Chart } from './chart';
import { PageSection } from './page-section';
export class TreeNode {
  id: string;
  title: string;
  charts: Array<Chart>;
  sections: Array<PageSection>;
  dashboardUrl: string;
  children: Array<TreeNode>;
  expanded = true;
  selected = false;
/*
  constructor(id, title, charts, children, expanded) {
    this.id = id;
    this.title = title;
    this.charts = charts;
    this.children = children;
    this.expanded = expanded;
  }
 */
}
