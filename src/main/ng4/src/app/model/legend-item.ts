export class LegendItem {
  key: string;
  value: string;
  isChecked: boolean;
  color: string;
  avgValue: number;
  stdDeviation: number;

  constructor(key: string, value: string, isChecked: boolean, color: string, avgValue: number, stdDeviation: number) {
    this.key = key;
    this.value = value;
    this.isChecked = isChecked;
    this.color = color;
    this.avgValue = avgValue;
    this.stdDeviation = stdDeviation;
  }
}
