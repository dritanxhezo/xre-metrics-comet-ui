import { DataSourceDefinition } from './chart-data-source-definition';
import { Setting } from './setting';
import { Population } from './population';
export class Chart {
    id: string;
    dataType: string;
    header: string;
    description: string;
    chartType: string;
    dataSources: Array<DataSourceDefinition>;
    options: {[optionKey: string]: string};
    settings: Array<Setting>;
    defaultTimeSpan: string;
    settingModel: string;
    mergeFunction = 'AVG';
    populations: Population[];
}
