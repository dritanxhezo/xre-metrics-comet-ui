import { Component, OnInit} from '@angular/core';
import {NgModule} from '@angular/core';
import {MatDialog} from '@angular/material';
import { CookieService } from 'ng2-cookies';
import { Favorite } from './model/favorite';
import { TreeNode } from './model/tree-node';
import { BackendService } from './service/backend.service';
import { LoginService } from './service/login.service';
import { EventService } from './service/event.service';
import { Constants } from './util/constants';
import { getDataSourcesType, updatePermission } from './util/util';
import { Permissions } from './util/permissions';
import { ActivatedRoute } from '@angular/router';
import { LoginDialogComponent } from './component/login-dialog/login-dialog.component';
import { Response, Headers } from '@angular/http';

@Component({
  selector: 'app-root',
  providers: [ CookieService ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  treeNodes: Array<TreeNode>;
  currentNode: TreeNode;
  buildInfo: string;
  userLogged = false;
  userName = 'Login';
  favorites: Array<Favorite>;
  population: string;
  populationList: Array<string>;
  currentReport: string;
  urlParams;
  permissions = new Permissions();

  constructor(public backendService: BackendService, private eventService: EventService, private loginService: LoginService, private constants: Constants, private cookieService: CookieService, public dialog: MatDialog) {
    eventService.onSelectReport.subscribe(treeNode => {
                                                        this.loadReport(treeNode);
                                                        this.currentReport = treeNode.id;
                                                      });
    eventService.onUrlPathChanged.subscribe(reportId => this.currentReport = reportId);
  }

  ngOnInit() {
    this.loadServerInfo();
    this.loadTreeNodes();
    this.favorites = this.backendService.favorites;
    this.loadPredefinedPopulations();
    this.constants.isABTestMode = this.cookieService.get(this.constants.COOKIES_IS_AB_TEST_MODE) === 'true' && this.userLogged;
  }

  loadServerInfo() {
    this.backendService.getVersion().subscribe( version => {this.buildInfo = version; },
                                                err =>     {console.log(err); });

  }

  loadTreeNodes() {
    this.backendService.getReports()
                          .subscribe( tn => {
                            this.treeNodes =  this.filterTreeNodes(tn);
                            this.loadDefaultReport();
                          },
                            err => { console.log(err); });
  }

  loadDefaultReport() {
    let tnReport = (this.currentReport) ? this.findReportById(this.currentReport) : this.findFirstReport(this.treeNodes);
    tnReport = tnReport ? tnReport : this.findFirstReport(this.treeNodes);
    // this.eventService.selectReport(tnReport);
    if (tnReport) { this.eventService.selectReport(tnReport); }
  }

  loadPredefinedPopulations() {
    this.backendService.getPredefinedPopulations().subscribe(popMap => {
      this.constants.populations = popMap;
      this.populationList = Object.keys(this.constants.populations);
    }, err => { console.log(err); });
  }

  findFirstReport(treeNodes: Array<TreeNode>) {
    for (let i = 0; i < treeNodes.length; i++) {
      if (treeNodes[i].children.length === 0) {
        return treeNodes[i];
      } else {
        return this.findFirstReport(treeNodes[i].children);
      }
    }
  }

  filterTreeNodes(treeNodes: Array<TreeNode>): Array<TreeNode> {
    const result = [];
    treeNodes.forEach(node => {
      const charts = node.charts ? node.charts.filter(chart =>  {
        const dataSourceType = getDataSourcesType(chart);
        return (!this.constants.isABTestMode && (dataSourceType === 1 || dataSourceType === 3)) ||
          (this.constants.isABTestMode && (dataSourceType === 2 || dataSourceType === 3));
      }) : [];
      const children = node.children ? this.filterTreeNodes(node.children) : [];
      if (charts.length > 0 || children.length > 0 || ((node.dashboardUrl || (node.sections && node.sections.length > 0)) && !this.constants.isABTestMode))  {
        const newNode = Object.assign({}, node);
        if (charts.length > 0 || children.length > 0) {
          newNode.charts = charts;
          newNode.children = children;
        }
        result.push(newNode);
      }
    });
    return result;
  }

  loadReportById(reportId: string) {
//    let foundNode;
//    for (const node of this.treeNodes) {
//      foundNode = this.findReportById(reportId, node);
//      if (foundNode) { break; }
//    }
    // this.loadReport(foundNode);
    const tnReport = this.findReportById(reportId);
    if (tnReport) { this.eventService.selectReport(tnReport); }
  }

  findReportById(reportId: string): TreeNode {
    let foundNode;
    for (const node of this.treeNodes) {
      foundNode = this.getReportById(reportId, node);
      if (foundNode) { return foundNode; }
    }
    return null;
  }

  getReportById(id, currentNode) {
    let result;

    if (id === currentNode.id) {
      return currentNode;
    } else {
      for (const child of currentNode.children) {
        result = this.getReportById(id, child);
        if (result !== null) {
          return result;
        }
      }
      return null;
    }
  }

  loadReport(treeNode: TreeNode) {
    if (this.currentNode) { this.currentNode.selected = false; }
    this.currentNode = treeNode;
    this.currentNode['selected'] = true;

//    if (this.currentNode.charts && this.currentNode.charts.length) {
//      if (this.currentNode.charts[0].dataType === 'CHART') {
//          this.eventService.loadChart(this.currentNode.charts[0]);
//      }
//      if (this.currentNode.charts[0].dataType === 'TABLE') {
//          this.eventService.loadTables(this.currentNode.charts);
//      }
//    }



//    if (this.isChartNode(treeNode)) {
//      this.eventService.loadChart(this.currentNode.charts[0]);
//    }
//    if (this.isTableNode(treeNode)) {
//      this.eventService.loadTables(this.currentNode.sections);
//    }

  }

  isTableNode(treeNode: TreeNode) {
    // return treeNode && treeNode.charts && treeNode.charts.find(chart => chart.dataType === 'TABLE');
    return treeNode && treeNode.sections;
  }

  isChartNode(treeNode: TreeNode) {
    // return treeNode && treeNode.charts && treeNode.charts.find(chart => chart.dataType === 'CHART');
    return treeNode && treeNode.charts;
  }

  showLogin() {
    if (this.userLogged === false) {
      this.dialog.open(LoginDialogComponent, {
        data: {
          user: 'AB Testing',
          parent: this
        }
      });
    }
  }

  login(loginResponse: any) {
    this.userLogged = true;
    this.userName = 'Undefined';
    if (loginResponse && loginResponse._body) {
      const loginObject = JSON.parse(loginResponse._body);
      if (loginObject.name) {
        this.userName = loginObject.name;
      }
      if (loginObject.authorities) {
        this.permissions = updatePermission(loginObject.authorities, this.constants);
      }
    }
    if (this.permissions.isABTestingAllowed) {
      const isMode = this.cookieService.get(this.constants.COOKIES_IS_AB_TEST_MODE) === 'true';
      if (this.constants.isABTestMode !== isMode) {
        this.constants.isABTestMode = isMode;
        if (this.constants.isABTestMode) {
          this.population = this.cookieService.get(this.constants.COOKIES_POPULATION);
          if (!this.population) {
            this.population = this.populationList[0];
          }
          this.constants.selectedPopolations = this.constants.populations[this.population];
        }
        this.loadTreeNodes();
      }
    }
  }

  logout() {
    this.loginService.logout().subscribe(logoutResponse => {
      console.log('Logout is successed: ' + logoutResponse);
    },
      err =>     {
      console.log('Logout error: ' + err);
    });
    this.permissions = new Permissions();
    this.userLogged = false;
    this.userName = 'Login';
    this.constants.isABTestMode = false;
    if (this.cookieService.get(this.constants.COOKIES_IS_AB_TEST_MODE) === 'true') {
      this.loadTreeNodes();
    }
  }

  removeFavorite(favorite) {
    this.backendService.removeFavorite(favorite);
  }

  setABTestMode() {
    this.constants.isABTestMode = !this.constants.isABTestMode;
    this.cookieService.set(this.constants.COOKIES_IS_AB_TEST_MODE, String(this.constants.isABTestMode));
    if (this.constants.isABTestMode) {
        this.population = this.cookieService.get(this.constants.COOKIES_POPULATION);
        if (!this.population) {
          this.population = this.populationList[0];
        }
        this.constants.selectedPopolations = this.constants.populations[this.population];
    }
    this.loadTreeNodes();
  }

  selectPopulation(populationName) {
    this.population = populationName;
    this.constants.selectedPopolations = this.constants.populations[populationName];
    this.cookieService.set(this.constants.COOKIES_POPULATION, populationName);
    this.loadDefaultReport();
  }
}
