import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import {MatDialog} from '@angular/material';
import {NoConflictStyleCompatibilityMode} from '@angular/material';

import { AppComponent } from './app.component';
import { OmitcolonPipe } from './util/omitcolon.pipe';
import { CapitalizePipe } from './util/capitalize.pipe';
import { ChartModule } from './component/chart/chart.module';
import { DateRangerModule } from './component/date-ranger/date-ranger.module';
import { TreeViewModule } from './component/tree-view/tree-view.module';
import { BackendService } from './service/backend.service';
import { LoginService } from './service/login.service';
import { SlideoutModule } from './component/slide-out/slide-out.module';
import { FilterComponent } from './component/filter/filter.component';
import { TableComponent } from './component/table/table.component';
import { PanelSliderComponent } from './component/panel-slider/panel-slider.component';
import { FilterArrayPipe } from './util/filter-array.pipe';
import { DateRangePickerModule } from './component/date-range-picker2/date-range-picker.module';
import { EventService } from './service/event.service';
import { Constants } from './util/constants';
import { TruncateCharactersPipe } from './util/truncate-characters.pipe';
import { TruncateWordsPipe } from './util/truncate-words.pipe';
import { PageNotFoundComponent } from './page-not-found.component';
import { ReportComponent } from './component/report/report.component';
import { SortPipe } from './util/sort.pipe';
import { LabelMultipleItemsPipe } from './util/label-multiple-items.pipe';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { TableFormatPipe } from './util/table-format.pipe';

const appRoutes: Routes = [
  { path: 'report/:id', component: ReportComponent },
  { path: '',           component: ReportComponent },
  { path: '**',         component: PageNotFoundComponent }
];

import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatTooltipModule, MatDialogModule} from '@angular/material';
import { LoginDialogComponent } from './component/login-dialog/login-dialog.component';

@NgModule({
    imports: [
              BrowserModule,
              FormsModule,
              HttpModule,
              JsonpModule,
              SlideoutModule,
              TreeViewModule,
              ChartModule,
              DateRangerModule,
              DateRangePickerModule,
              NoopAnimationsModule,
              MatTooltipModule,
              MatDialogModule,
              NoConflictStyleCompatibilityMode,
              RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  declarations: [
              AppComponent,
              OmitcolonPipe,
              CapitalizePipe,
              FilterComponent,
              TableComponent,
              PanelSliderComponent,
              FilterArrayPipe,
              TruncateCharactersPipe,
              TruncateWordsPipe,
              ReportComponent,
              PageNotFoundComponent,
              SortPipe,
              LabelMultipleItemsPipe,
              DashboardComponent,
              TableFormatPipe,
              LoginDialogComponent
  ],
  providers: [BackendService, EventService, LoginService, Constants],
  bootstrap: [AppComponent],
  entryComponents: [LoginDialogComponent]
})
export class AppModule { }
