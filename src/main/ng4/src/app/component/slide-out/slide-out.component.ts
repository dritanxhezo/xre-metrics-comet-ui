import { Component, Input, AfterViewInit, HostListener, ElementRef, ViewChild, OnInit } from '@angular/core';
import { EventService } from '../../service/event.service';
declare var $: any;

@Component({
  selector: 'slide-out',
  templateUrl: './slide-out.component.html',
  styleUrls: ['./slide-out.component.css']
})
export class SlideoutComponent {
  @Input() direction: string;
  @Input() floating = true;
  @Input() icon: string;
  @ViewChild('slideout') slideout: ElementRef;

  constructor(private el: ElementRef, private eventService: EventService) {
    eventService.onCloseSlideout.subscribe(() => this.toggleVisibility());
  }

  toggleVisibility() {
    const panel = $(this.slideout.nativeElement);
    if (panel.hasClass('visible')) {
      const panelSlide = -1 * parseInt(panel.outerWidth(true));
      if (this.direction === 'left') { panel.removeClass('visible').animate({ 'margin-left': panelSlide + 'px' }, 'fast'); }
      if (this.direction === 'right') { panel.removeClass('visible').animate({ 'margin-right': panelSlide + 'px' }, 'fast'); }
      this.icon = 'glyphicon-chevron-right';
    } else {
      if (this.direction === 'left') { panel.addClass('visible').animate({ 'margin-left': '0px' }, 'fast'); }
      if (this.direction === 'right') { panel.addClass('visible').animate({ 'margin-right': '0px' }, 'fast'); }
      this.icon = 'glyphicon-chevron-left';
    }
  }
}
