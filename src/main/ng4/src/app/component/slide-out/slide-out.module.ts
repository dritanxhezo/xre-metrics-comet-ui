import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { SlideoutComponent } from './slide-out.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [
    SlideoutComponent,
  ],
  providers: [],
  exports: [SlideoutComponent]
})
export class SlideoutModule { }
