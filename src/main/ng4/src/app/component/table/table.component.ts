import { Chart } from '../../model/chart';
import { PageSection } from '../../model/page-section';
import { TableDefinition } from '../../model/table-definition';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import {FilterArrayPipe} from '../../util/filter-array.pipe';

@Component({
  selector: 'table-component',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {

  sectionDatas = new Array<PageSection>();

  constructor(private el: ElementRef, private backendService: BackendService, private eventService: EventService) {
    eventService.onLoadTableData.subscribe(sections => this.loadPageData(sections));
  }

  ngOnInit() {
  }

  loadPageData(sections: Array<PageSection>) {
    this.sectionDatas = sections;
    this.sectionDatas.forEach(sectionData => this.backendService.getTableValues(sectionData['tableDefinition']['dataSource'])
                                                       .subscribe(data => { sectionData['data'] = data; console.log(sectionData);},
                                                                   err => { console.log(err); }));
  }

  getFields(row) {
    return Object.keys(row);
  }

  sortBy(sectionData, colName, colType) {
    const currentSortingOrder = this.getCurrentSortingOrder(sectionData, colName);
    const nextSortingOrder = (currentSortingOrder.length && (currentSortingOrder === 'asc')) ? 'desc' : 'asc';   // toggle the sorting order
    sectionData['tableDefinition']['sortedField'] = colName;
    sectionData['tableDefinition']['sortedOrder'] = nextSortingOrder;
    if (nextSortingOrder === 'asc') {
      sectionData.data.sort((a, b) => a[colName].localeCompare(b[colName], 'en', {numeric: true}));
    } else {
      // desc is never the first sorting order, which means if nextSortingOrder is 'desc', then the current column must already be sorted as asc
      // in that case we simply do a reverse since it is much faster.
      sectionData.data.reverse();
    }
  }

  getCurrentSortingOrder(sectionData, colName) {
    return (sectionData['tableDefinition']['sortedField'] && (sectionData['tableDefinition']['sortedField'] === colName) && sectionData['tableDefinition']['sortedOrder']) ? sectionData['tableDefinition']['sortedOrder'] : '';
  }

  hasPagination() {
    return false;
  }

}
