import { DateRange } from '../../model/date-range';
import { formatDate, convertUTCDateToLocal } from '../../util/util';
import { Component, Output, EventEmitter, ElementRef, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
declare var $: any;
declare var moment: any;

@Component({
  selector: 'date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.css']
})
export class DateRangePickerComponent implements OnChanges {

  @Input() parentOptions;
  @Output() rangeUpdated = new EventEmitter();
  dateLabel: string;

  @ViewChild('dateRangePicker') dateRangePicker: ElementRef;

  private options = {
      format: 'M/D/YYYY H:mm',
      separator: ' - ',
      getValue: function() {
         // return $(this).val();
         // return this.dateLabel;
      },
      setValue: function(s) {
        // if (!$(this).attr('readonly') && !$(this).is(':disabled') && s !== $(this).val()) {
        //   $(this).val(s);
        // }
        // this.dateLabel = s;
      },
      start: {},
      end: {},
      time: {
        enabled: true
      },
      beforeShowDay: function(t) {
        const valid = moment(t).isAfter(moment().subtract(30, 'days')) && moment(t).isBefore(moment().add(1, 'days'));
        const _class = '';
        const _tooltip = valid ? '' : 'No data available';
        return [valid, _class, _tooltip];
      },
      showShortcuts: true,
      selectedShortcut: 'last_1_hr',
      customShortcuts : [
        { name: 'last_1_hr',      label: 'Past Hour',      autoClose: true, dates: function() {return [moment().subtract(1, 'hours').toDate(),                moment().subtract(1, 'minutes').toDate()]; }},
        { name: 'last_12_hrs',    label: 'Last 12 Hours',  autoClose: true, dates: function() {return [moment().subtract(12, 'hours').toDate(),               moment().toDate()]; }},
        { name: 'last_primetime', label: 'Last Primetime', autoClose: true, dates: function() {return [moment().startOf('day').subtract(5, 'hours').toDate(), moment().startOf('day').add(2, 'hours').toDate()]; }},
        { name: 'last_reboot',    label: 'Last Reboot',    autoClose: true, dates: function() {return [moment().startOf('day').add(3, 'hours').toDate(),      moment().startOf('day').add(7, 'hours').toDate()]; }},
        { name: 'today',          label: 'Today',          autoClose: true, dates: function() {return [moment().utc().startOf('day').toDate(),                moment().toDate()]; }},
        { name: 'yesterday',      label: 'Yesterday',      autoClose: true, dates: function() {return [moment().utc().startOf('day').subtract(1, 'days').toDate(),  moment().utc().startOf('day').toDate()]; }},
        { name: 'last_7_days',    label: 'Last 7 Days',    autoClose: true, dates: function() {return [moment().utc().startOf('day').subtract(7, 'days').toDate(),  moment().utc().startOf('day').toDate()]; }},
        { name: 'last_30_days',   label: 'Last 30 Days',   autoClose: true, dates: function() {return [moment().utc().startOf('day').subtract(30, 'days').toDate(), moment().utc().startOf('day').toDate()]; }},
        { name: 'custom_range',   label: 'Custom Range',  autoClose: false, dates: function() {return [moment().startOf('day').subtract(1, 'days').toDate(),  moment().toDate()]; }},
      ],
      stickyMonths: true,
      selectForward: false,
      selectBackward: false,
      customArrowPrevSymbol: '<i class="glyphicon glyphicon-chevron-left"></i>',
      customArrowNextSymbol: '<i class="glyphicon glyphicon-chevron-right"></i>',
      monthSelect: true,
      yearSelect: true
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes.parentOptions) {
      Object.assign(this.options, this.parentOptions);
      this.dateLabel = formatDate(this.parentOptions.start) + ' - ' + formatDate(this.parentOptions.end);

      $(this.dateRangePicker.nativeElement).dateRangePicker(this.options)
                                           .bind('datepicker-change', {comp: this}, function(ev, picker) {
                                               let rangeStart = picker.date1, rangeEnd = picker.date2;
                                               if (picker.selectedShortcut === 'custom_range') {
                                                    rangeStart = convertUTCDateToLocal(picker.date1);
                                                    rangeEnd = convertUTCDateToLocal(picker.date2);
                                               }
                                               ev.data.comp.dateLabel = formatDate(rangeStart) + ' - ' + formatDate(rangeEnd);
                                               ev.data.comp.rangeUpdated.emit({start: rangeStart, end: rangeEnd, selectedShortcut: picker.selectedShortcut});
                                           });
    }
  }
}
