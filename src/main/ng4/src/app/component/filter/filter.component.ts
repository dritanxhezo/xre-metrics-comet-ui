import { Chart } from '../../model/chart';
import { DateRange } from '../../model/date-range';
import { Filter } from '../../model/filter';
import { KeyValue } from '../../model/key-value';
import { Setting } from '../../model/setting';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';
import { Constants } from '../../util/constants';
import { Component, AfterViewInit, Output} from '@angular/core';
import { openModal, closeModal, formatDate } from '../../util/util';
import { SortPipe } from '../../util/sort.pipe';
import {LabelMultipleItemsPipe} from '../../util/label-multiple-items.pipe';

declare var moment: any;

@Component({
  selector: 'filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
})
export class FilterComponent implements AfterViewInit {

  currentChart: Chart;
  currentSetting: Setting;
  currentFilterSettingValues: Array<string>;
  groupParamSetting: Setting;
  dataParamSettings: Array<Setting>;
  groupBy: string;
  settingSortedField = '-uiGroup';  // sorted descending
  filters: Array<Filter>;
  refresh: any;

  dateRange = new DateRange(1, moment().subtract(1, 'hours').toDate(), moment().toDate());
  dateRangePickerOptions;
  uiDateRangePickerOptions = {
    start: this.dateRange.min,
    end: this.dateRange.max,
    visibleShortcuts: ['last_1_hr', 'last_12_hrs' ]
  };
  apiUiDateRangePickerOptions  = {
    start: this.dateRange.min,
    end: this.dateRange.max,
    visibleShortcuts: ['last_1_hr', 'last_12_hrs', 'last_primetime', 'last_reboot', 'today', 'yesterday', 'last_7_days', 'last_30_days', 'custom_range']
  };

  dateRangeGranularity = {last_1_hr: 1,
                          last_12_hrs: 1,
                          last_primetime: 1,
                          last_reboot: 1,
                          today: 15,
                          yesterday: 15,
                          last_7_days: 60,
                          last_30_days: 1440};

  constructor(private backendService: BackendService, private eventService: EventService, private constants: Constants) {
    eventService.onChartSelected.subscribe(chart => this.currentChart = chart);
    eventService.onRangeUpdated.subscribe(dateRange => this.dateRange = dateRange);
    eventService.onUrlPathParamsChanged.subscribe(filters => this.filters = filters);
    eventService.onLoadChartFilters.subscribe(() => this.loadChartFilter());
  }

  ngAfterViewInit() {
//    this.dateRangePickerOptions['start'] = moment(this.dateRange.min).utc().toDate();
//    this.dateRangePickerOptions['end'] = moment(this.dateRange.max).utc().toDate();
  }

//  mergeFilters(filters: Array<Filter>) {
//    console.log('filters ' + JSON.stringify(filters));
//    if (this.currentChart && this.currentChart.settings) {
//      this.currentChart.settings.forEach(setting => {
//        console.log('mergeFilters: ' + setting.selectedLabel + ':::' + setting.selectedValue + ':::' +  filters[setting.id]);
//        if (filters[setting.id]) {
//        // setting.selectedLabel = setting.selectedValue = filters[setting.id];
//            setting.selectedValue = filters[setting.id];
//        }
//      });
//    }
//  }

  loadChartFilter() {
//    if (this.backendService.getPrefs('deepLinkEnabled').toUpperCase() === 'true'.toUpperCase()) {
//      if (this.filters) { this.mergeFilters(this.filters); this.filters = null; }
//    }
    this.loadSettingValues();
//    if (this.currentChart.settings) {
//      this.currentChart.settings.forEach(setting => this.setSelectedLabel(setting));
//    }
    this.setSettingGroups();
//    this.currentSetting = (this.dataParamSettings && this.dataParamSettings.length > 0) ? this.dataParamSettings[0] : null;
    this.dateRangePickerOptions = this.currentChart.settingModel === 'API_UI' ? this.apiUiDateRangePickerOptions : this.uiDateRangePickerOptions;
  }

  loadSettingValues() {
    if (this.currentChart.settings) {
      if (this.currentChart.settingModel === 'API_UI') {
        this.loadApiSettingValues();
      }
      if (this.currentChart.settingModel === 'UI') {
        this.loadUISettingValues();
      }
    }
  }

  loadUISettingValues() {
    for (const setting of this.currentChart.settings) {
      const filterStr = this.getFilterString(setting, this.currentChart.settings);
      let dataSource = setting.dataSource + this.appendToUrl(setting.dataSource, filterStr);
      if (!dataSource.includes('groupParamValue')) { dataSource += this.appendToUrl(dataSource, 'groupParamValue=ALL'); }
      if (!dataSource.includes('dataParam')) { dataSource += this.appendToUrl(dataSource, 'dataParam=ALL'); }

      this.backendService.getSettingValues(dataSource)
                         .subscribe(settingValues => { this.mapToKeyValueArray(setting, settingValues);
                                                       this.setSettingGroups();
                                                       this.currentSetting = (this.dataParamSettings && this.dataParamSettings.length > 0) ? this.dataParamSettings[0] : null;
                                                     },
                         err => { console.log(err); });
    }
  }

  appendToUrl(url: string, param: string) {
    return (url.includes('?') ? '&' : '?') + param;
  }

  getFilterString(forSetting: Setting, settings: Array<Setting>) {
    let filterStr = '';
    for (const setting of settings) {
      filterStr += (filterStr.length ? '&' : '') +
                   (setting.uiGroup === 'groupParam' ? 'groupParamValue' : setting.uiGroup) + '=' +
                   (this.isDataParam(forSetting) && this.isGroupParam(setting) ? 'ALL' : setting.selectedValue);
    }
    console.log('filterStr ' + filterStr);
    return filterStr;
  }

  mapToKeyValueArray(setting: Setting, mapObj: Map<string, string>) {
    const selectedValue = setting.selectedValue.split('||');
    setting.valuesList = [];

    if (mapObj != null) {
      Object.keys(mapObj).map(key => setting.valuesList.push(new KeyValue(key, mapObj[key], selectedValue.includes(key))));
    }
    this.addAdditionalItems(setting);
    this.moveToFirst(setting.valuesList, 'ALL');
    this.setSelectedLabel(setting);
    this.checkSelected(setting);
  }

  loadApiSettingValues() {
    openModal();
    const dynamicValueIndex = this.currentChart.settings.findIndex(sett => sett['type'] === 'DYNAMIC_VALUE');
    if (dynamicValueIndex > -1) { this.currentSetting = this.currentChart.settings[dynamicValueIndex]; }
    const settingId = (this.currentSetting) ? this.currentSetting.id : null;
    const dateRange = this.currentChart.defaultTimeSpan ? this.dateRange : null;
    this.backendService.getApiSettingValues(this.currentChart.id, this.currentChart.settings, settingId, dateRange)
                              .subscribe(allSettingValues => this.assignAllSettingValues(allSettingValues),
                                err => { console.log(err); closeModal(); },
                                () =>  { closeModal(); });
  }

  assignAllSettingValues(allSettingValues: Array<Setting>) {
    let currentSettings = Object.assign([], this.currentChart.settings);
    allSettingValues.forEach(setting => {
                                            let currSetting = currentSettings.find(sett => sett.id === setting.id);
                                            if (!currSetting) { currentSettings.push(setting); currSetting = setting; }
                                            currSetting.valuesList = setting['items'];
                                            this.addAdditionalItems(currSetting);
                                            this.moveToFirst(currSetting.valuesList, 'ALL');
                                            if (!setting.defaultValue) { setting.defaultValue = 'ALL'; }
                                            this.setSelectedLabel(setting);
                                            this.checkSelected(currSetting);
                                            setting.name = setting.friendlyName;
                                            if (setting.name.lastIndexOf(':') !== setting.name.length - 1) { setting.name = setting.friendlyName + ':'; }
                                        } );
    currentSettings = this.intersectOnId(currentSettings, allSettingValues);
    this.currentChart.settings = currentSettings;
    this.setSettingGroups();
    this.currentSetting = (this.dataParamSettings && this.dataParamSettings.length > 0) ? this.dataParamSettings[0] : null;
  }

  addAdditionalItems(setting: Setting) {
    if (setting.additionalItems) {
      if (!setting.valuesList) { setting.valuesList = []; }
      setting.additionalItems.forEach(addtlItem => {
        if (!setting.valuesList.find(val => val.itemLabel === addtlItem.itemLabel)) {
          setting.valuesList.push(addtlItem);
        }
      });
    }
  }

  moveToFirst(valuesList, itemValue) {
    const itemIndex = valuesList.findIndex(elm => elm.itemValue.toUpperCase() === itemValue);
    if (itemIndex > 0) {
      const allItem = valuesList[itemIndex];
      valuesList.splice(itemIndex, 1);
      valuesList.splice(0, 0, allItem);
    }
  }

  checkSelected(setting: Setting) {
    if (setting.selectedValue) {
      const selectedValues = setting.selectedValue.split('||');
      if (selectedValues.includes('ALL')) {
        if (setting.valuesList.length) {
          setting.valuesList.forEach(settValue => settValue.isChecked = true);
        }
      } else {
        const allItem = setting.valuesList.find(sett => sett.itemValue.toUpperCase() === 'ALL');
        if (allItem) {
          if (selectedValues.length === this.getItemsOtherThanAll(setting).length) {
            allItem.isChecked = true;
          } else {
            allItem.isIndeterminate = true;
          }
        }
        setting.valuesList.filter(settValue => selectedValues.indexOf(settValue.itemValue) > -1).forEach(settValue => settValue.isChecked = true);
      }
    }
  }

  intersectOnId(s1: Array<Setting>, s2: Array<Setting>) {
    return s1.filter(function (s1elm) {
        return s1elm.type === 'STATIC_FIELD_VALUES' || s2.filter(s2elm => s2elm.id === s1elm.id).length > 0;
    });
  }

  setSettingGroups() {
    this.groupParamSetting = (this.currentChart.settings) ? this.currentChart.settings.find(this.isGroupParam) : null;
    this.dataParamSettings = (this.currentChart.settings) ? this.currentChart.settings.filter(this.isDataParam) : null;
    this.groupBy = this.groupParamSetting ? this.groupParamSetting.selectedValue : null;
  }

  setSelectedLabel(setting: Setting) {
    setting.selectedLabel = setting.selectedValue = (setting.selectedValue ? setting.selectedValue : this.getDefaultValue(setting));
  }

  getDefaultValue(setting: Setting): string {
    if (setting.defaultValue) {
      return setting.defaultValue;
    } else {
      if (setting.valuesList && setting.valuesList.length) {
        return setting.valuesList[0].itemValue;
      } else {
        return '';
      }
    }
  }

  onSettingsChanged(setting: Setting) {
    this.currentSetting = setting;
    this.currentFilterSettingValues =  this.currentSetting.selectedValue.split('||');
    this.currentChart.settings.find(chartSetting => chartSetting.id === this.currentSetting.id).selectedValue = this.currentSetting.selectedValue;
  }

  onSettingsValChanged(settingValue: KeyValue) {
    if (this.currentSetting.multiSelect) {
      const allItem = this.currentSetting.valuesList.find(sett => sett.itemValue.toUpperCase() === 'ALL');
      if (settingValue === allItem) {
        this.currentSetting.valuesList.map(val => val.isChecked = allItem.isChecked);
        allItem.isIndeterminate = false;
      } else {
        const itemsOtherThanAll = this.getItemsOtherThanAll(this.currentSetting);
        const checkedItems = itemsOtherThanAll.filter(val => val.isChecked);
        const allChecked = ( itemsOtherThanAll.length === checkedItems.length );
        allItem.isChecked = allChecked;
        allItem.isIndeterminate = (!allChecked) && (checkedItems.length > 0);
      }
      this.currentSetting.selectedValue = allItem.isChecked ? 'ALL' : (allItem.isIndeterminate ? this.getFilterSettingValue(this.currentSetting) : 'NONE');
    }
    this.currentSetting.selectedLabel = this.getFilterSettingValueLabel(this.currentSetting);
    this.currentChart.settings.find(chartSetting => chartSetting.id === this.currentSetting.id).selectedValue = this.currentSetting.selectedValue;
    this.autoRefresh();
  }

  getFilterSettingValue(setting: Setting) {
      return setting.valuesList.filter(val => val.isChecked).map(val => val.itemValue).join('||');
  }

  getFilterSettingValueLabel(setting: Setting) {
    if (!setting.valuesList) { return ''; }
    if (setting.multiSelect) {
      const allItem = setting.valuesList.find(sett => sett.itemValue.toUpperCase() === 'ALL');
      const checkedItems = this.getItemsOtherThanAll(setting).filter(val => val.isChecked);
      let checkedLabel = checkedItems.map(val => val.itemLabel).join('||');
//      if (checkedLabel.length > 40) {
//        checkedLabel = checkedItems.length + ' items selected';
//      }
      // return allItem.isChecked ? 'ALL' : ((!allItem.isChecked) && (checkedItems.length > 0) ? checkedLabel : 'NONE');
      return (allItem && allItem.isChecked) ? 'ALL' : ((!(allItem && allItem.isChecked)) && (checkedItems.length > 0) ? checkedLabel : 'NONE');
    } else {
      const selectedItem = setting.valuesList.find(item => item.itemValue === setting.selectedValue);
      return (selectedItem && selectedItem.itemLabel) ? selectedItem.itemLabel : setting.selectedValue;
    }
  }

  getItemsOtherThanAll(setting: Setting) {
    return setting.valuesList.filter(val => val.itemValue !== 'ALL');
  }

  resetSetting(setting: Setting) {
      setting.selectedLabel = setting.selectedValue = this.getDefaultValue(setting);
      const defaultValues = this.getDefaultValue(setting).split('||');
      setting.valuesList.map(val => { val.isChecked = defaultValues.includes(val.itemValue) || defaultValues.includes('ALL');
                                      val.isIndeterminate = (val.itemValue === 'ALL' && !val.isChecked); });

//    setting.valuesList.forEach(val => { val.isChecked = false; val.isIndeterminate = false; });
//    const selectedValue = this.getDefaultValue(setting);
//    const settingValue = setting.valuesList.find(val => val.itemValue === selectedValue);
//    settingValue.isChecked = true;
//    settingValue.isIndeterminate = false;
//    this.onSettingsValChanged(settingValue);
  }

  getGroupSettingValueLabel(setting: Setting) {
     return setting.valuesList.find(val => (val.itemValue === setting.selectedValue)).itemLabel;
  }

  applyFilter() {
    if (this.currentChart.settingModel === 'API_UI') {  // refresh the drop downs
      this.loadSettingValues();
      this.groupParamSetting = (this.currentChart.settings) ? this.currentChart.settings.find(this.isGroupParam) : null;
      this.dataParamSettings = (this.currentChart.settings) ? this.currentChart.settings.filter(this.isDataParam) : null;
    }
    const resetLegendSelection = this.groupParamSetting && (!this.groupBy || this.groupParamSetting.selectedValue !== this.groupBy);
    this.groupBy = this.groupParamSetting ? this.groupParamSetting.selectedValue : null;
    this.eventService.loadChartData(resetLegendSelection);
    this.eventService.setUrlFromFilter();
  }

  isGroupParam(setting: Setting): boolean {
     return ['groupParam', 'group'].includes(setting.uiGroup);
  }

  isDataParam(setting: Setting): boolean {
    return !(['groupParam', 'group'].includes(setting.uiGroup));
  }

  stopMenuClose(event: Event) {
    event.stopPropagation();
  }

  onDateRangeUpdated(dateRangePickerVal) {
    this.dateRange.min = dateRangePickerVal.start;
    this.dateRange.max = dateRangePickerVal.end;
    if (dateRangePickerVal.selectedShortcut && this.dateRangeGranularity[dateRangePickerVal.selectedShortcut]) {
      this.dateRange.period = this.dateRangeGranularity[dateRangePickerVal.selectedShortcut];
    } else {
      if (!this.showGranularity(this.constants['MIN' + this.dateRange.period])) {
        this.dateRange.period = this.calculateGranularity();
      }
    }
    this.eventService.rangeUpdated(this.dateRange);
  }

  showGranularity(granularity) {
    if (this.currentChart.settingModel === 'UI') {
      return granularity === this.constants.MIN1;
    }
    return moment(this.dateRange.max).diff(moment(this.dateRange.min), 'minutes') <= granularity.AVAILABLE_RANGE;
  }

  calculateGranularity() {
    for (const gran of [1, 15, 60, 1440]) {
      if (this.showGranularity(this.constants['MIN' + gran])) { return gran; }
    }
    return 0;
  }

  setPeriod() {
    this.eventService.rangeUpdated(this.dateRange);
  }

  timeFrameLabel(): string {
    return (formatDate(this.dateRange.min) + ' - ' + formatDate(this.dateRange.max));
  }

  resetAllSettings() {
    this.currentChart.settings.forEach(setting => this.resetSetting(setting));
    this.applyFilter();
  }

  autoRefresh() {
    //restart the refresh timeout on each checkbox click
    clearTimeout(this.refresh);
    this.refresh = setTimeout(() => { this.applyFilter() }, 1500);
  }

}
