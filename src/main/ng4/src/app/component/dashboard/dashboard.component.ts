import { EventService } from '../../service/event.service';
import { Component, OnInit } from '@angular/core';
declare var Dashboard: any;

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  dashboard;

  constructor(private eventService: EventService) {
    eventService.onLoadDashboard.subscribe(dataUrl => this.startDashboard(dataUrl));
    eventService.onUnLoadDashboard.subscribe(dataUrl => this.stopDashboard());
  }

  ngOnInit() {
  }

  startDashboard(dataUrl: string) {
    this.dashboard = new Dashboard('dashboard-container');
    this.dashboard.start('/proxy' + dataUrl);
  }

  stopDashboard() {
    if (this.dashboard) { this.dashboard.stop(); this.dashboard = null; }
  }
}
