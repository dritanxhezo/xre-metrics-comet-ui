import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { TreeViewComponent } from './tree-view.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [
    TreeViewComponent,
  ],
  providers: [],
  exports: [TreeViewComponent]
})
export class TreeViewModule { }
