import { Favorite } from '../../model/favorite';
import { Component, Input, EventEmitter } from '@angular/core';
import { TreeNode } from '../../model/tree-node';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';

@Component({
  selector: 'tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})

export class TreeViewComponent {
  @Input() treeNodes: Array<TreeNode>;

  constructor(private eventService: EventService, private backendService: BackendService) { }

  nodeClicked(treeNode: TreeNode) {
    if (treeNode.children.length) {
      treeNode.expanded = !treeNode.expanded;
    } else {
      this.eventService.selectReport(treeNode);
      this.eventService.closeSlideout();
    }
  }

  isFavorite(treeNodeId: string) {
    return (this.backendService.getFavoriteIndex(treeNodeId) >= 0);
  }

  toggleFavorite(treeNode: TreeNode) {
    const fav = new Favorite();
    fav.id = treeNode.id;
    fav.label = treeNode.title;
    if (this.backendService.getFavoriteIndex(treeNode.id) === -1) {
      this.backendService.addFavorite(fav);
    } else {
      this.backendService.removeFavorite(fav);
    }
  }
}
