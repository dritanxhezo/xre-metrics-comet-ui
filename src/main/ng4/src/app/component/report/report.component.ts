import { Chart } from '../../model/chart';
import { Filter } from '../../model/filter';
import { TreeNode } from '../../model/tree-node';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  currentNode: TreeNode;
  pageIndex: number;
  currentChartId: string;
  filters: Array<Filter> = [];
  currentUrlPath: string;
  autorefresh: boolean = false;
  autoRefreshInterval: any;

  constructor(private eventService: EventService, private backendService: BackendService, private route: ActivatedRoute, private location: Location) {
    eventService.onSelectReport.subscribe(treeNode => this.loadReport(treeNode));
    eventService.onChartSelected.subscribe(chart => this.chartSelected(chart));
    eventService.onAppliedFilters.subscribe(() => this.updateUrlParams());
  }

  ngOnInit() {
    if (this.backendService.getPrefs('deepLinkEnabled').toUpperCase() === 'true'.toUpperCase()) {
      if (this.currentUrlPath !== this.location.path()) {
        const currentReport = this.route.snapshot.params['id'];
        const filters = Object.assign([], this.route.snapshot.queryParamMap['params']);
        if (filters) {Object.keys(filters).forEach(key => filters[key] = filters[key].replace(/\,/g, '||')); }
        this.pageIndex = filters['page'] ? Number(filters['page']) : 0;
        this.currentUrlPath = this.location.path();

        if (currentReport) { this.eventService.selectReportByName(currentReport); }
        if (filters) {
          this.filters = filters;
          // this.eventService.setReportFiltersByUrl(filters);
        }
      }
    }
  }

  mergeFilters(chart: Chart, filters: Array<Filter>) {
    console.log('filters ' + JSON.stringify(filters));
    if (chart && chart.settings && filters) {
      chart.settings.forEach(setting => {
          if (filters[setting.id]) { setting.selectedValue = filters[setting.id]; }
      });
    }
    this.filters = [];
  }

  setURL(reportId: string, filters: Array<Filter>) {
    if (this.backendService.getPrefs('deepLinkEnabled').toUpperCase() === 'true'.toUpperCase()) {
      const params = (filters) ? filters.join('&') : '';
      console.log('setURL: ' + reportId + '?' + params);
      this.location.go('/report/' + reportId, params.replace(/\|\|/g, ','));
    }
  }

  updateUrlParams() {
//    if (this.backendService.getPrefs('deepLinkEnabled').toUpperCase() === 'true'.toUpperCase()) {
      let filters = [];
      if (this.currentNode.charts[this.pageIndex].settings) {
        filters = this.currentNode.charts[this.pageIndex].settings.map(setting => new Filter(setting.id, setting.selectedValue));
      }
      filters.push(new Filter('page', this.pageIndex + ''));
      this.setURL(this.currentNode.id, filters);
//      const params = (filters) ? filters.join('&') : '';
//      console.log('setURL: ' + this.currentNode.id + '?' + params);
//      this.location.go('/report/' + this.currentNode.id, params.replace(/\|\|/g, ','));
//    }
  }

  loadReport(treeNode: TreeNode) {
    this.currentNode = treeNode;
    if (this.isChartNode(treeNode)) {
      if (!this.pageIndex || (this.pageIndex < 0) || (this.pageIndex > (this.currentNode.charts.length - 1))) { this.pageIndex = 0; };
      // this.currentChartId = this.currentNode.charts[this.pageIndex].id;
      this.eventService.loadChart(this.currentNode.charts[this.pageIndex]);
    }
    if (this.isTableNode(treeNode)) {
      this.eventService.loadTables(this.currentNode.sections);
      this.setURL(this.currentNode.id, []);
    }
    if (this.isDashboardNode(treeNode)) {
      this.eventService.loadDashboard(treeNode.dashboardUrl);
      this.setURL(this.currentNode.id, []);
    } else {
      this.eventService.unLoadDashboard();
    }
  }

  chartSelected(chart: Chart) {
    this.pageIndex = this.currentNode.charts.findIndex(ch => ch.id === chart.id);
    // if (this.currentChartId === chart.id) {
      this.mergeFilters(this.currentNode.charts[this.pageIndex], this.filters);
    // } else {
    //  this.filters = [];
    // }
    this.updateUrlParams();
    // this.eventService.selectPage(this.pageIndex);
  }

  isChartNode(treeNode: TreeNode) {
    return treeNode && treeNode.charts;
  }

  isTableNode(treeNode: TreeNode) {
    return treeNode && treeNode.sections;
  }

  isDashboardNode(treeNode: TreeNode) {
    return treeNode && treeNode.dashboardUrl;
  }

  onAutorefreshUpdated() {
    this.autorefresh = !this.autorefresh;
    if(this.autorefresh){
      this.eventService.autoRefresh();
      const refreshChart = function (){ this.eventService.autoRefresh() }.bind(this);
      this.autoRefreshInterval = setInterval(refreshChart, 60000);
    } else {
      clearInterval(this.autoRefreshInterval)
    }
  }

}
