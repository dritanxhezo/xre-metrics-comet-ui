import { Chart } from '../../model/chart';
import { DateRange } from '../../model/date-range';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';
import { Constants } from '../../util/constants';
import { isUndefined, convertTimeSpanToDuration } from '../../util/util';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, AfterViewChecked } from '@angular/core';

declare var $: any;
declare var moment: any;

@Component({
  selector: 'date-ranger',
  templateUrl: './date-ranger.component.html',
  styleUrls: ['./date-ranger.component.css']
})
export class DateRangerComponent implements OnInit, AfterViewInit, AfterViewChecked {

  dateRange: DateRange;
  hasDateRange = false;
  dateLabels = new Array<DateLabels>();
  rangeMin: number;
  rangeMax: number;
  currPos: number;
  updateCursors = false;
  dateFormat = 'DD/MM/YYYY HH:mm:ss';
//  dateRangePickerOptions = {opens: 'right', drops: 'up', parentEl: 'dateRangePicker_button'};


  @ViewChild('dateRanger') dateRanger: ElementRef;
  @ViewChild('cursorMin') cursorMin: ElementRef;
  @ViewChild('cursorMax') cursorMax: ElementRef;
  @ViewChild('guide') guide: ElementRef;
  @ViewChild('config') config: ElementRef;

  private options = {
    'showDropdowns': true,
    'timePickerIncrement': 5,
    'timePicker': true,
    'timePicker24Hour': true,
    'autoApply': true,
    'autoUpdateInput': true,
    'opens': 'left',
    'drops': 'up',
    'buttonClasses': 'btn btn-sm',
    'applyClass': 'btn-success',
    'cancelClass': 'btn-default',
    'startDate': moment().subtract(12, 'hours'),
    'endDate': moment(),
    'minDate': moment().subtract(29, 'days'),
    'maxDate': moment(),
    'ranges': {
       'Last 12hr': [moment().subtract(12, 'hours'), moment()],
       'Last Primetime': [moment().startOf('day').subtract(5, 'hours'), moment().startOf('day').add(2, 'hours')],
       'Last Reboot':  [moment().startOf('day').add(2, 'hours'), moment().startOf('day').add(4, 'hours')],
       'Today': [moment().startOf('day'), moment()],
       'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  };

  constructor(private eventService: EventService, private backendService: BackendService, private constants: Constants) {
    eventService.onChartSelected.subscribe(chart => this.updatePeriod(chart));
  }

  ngOnInit() {
    this.loadDateLabels1();
  };

//  loadDateLabels() {
//    const period = (this.dateRange && this.dateRange.period) ? this.dateRange.period : 1;
//    this.updateLabels(period);
//    this.dateRange = new DateRange(period, this.dateLabels[this.rangeMin].dt, this.dateLabels[this.rangeMax].dt);
//  }

  loadDateLabels1() {
    const period = (this.dateRange && this.dateRange.period) ? this.dateRange.period : 1;
    const end = new Date();
    const start = new Date(end.getTime() - this.constants['MIN' + period].AVAILABLE_RANGE * 60000);
    this.dateRange = new DateRange(period, start, end);
    this.updateSliderLabels();
  }

  setPeriod() {
//    this.updateLabels(this.dateRange.period);
//    this.dateRange.min = this.dateLabels[this.rangeMin].dt;
//    this.dateRange.max = this.dateLabels[this.rangeMax].dt;
    this.updateSliderLabels();
    this.eventService.rangeUpdated(this.dateRange);
//
//    this.guide.nativeElement.style.width = (this.dateLabels.length * 30) + 'px';  // 30 is the width of block, replace the constant
//    this.updateCursors = true;
  }

  updatePeriod(chart: Chart) {
    if (chart.settings) {
//      const timeSpanSetting = chart.settings.find(setting => setting.id === 'timeSpan');
//      this.hasDateRange = !isUndefined(timeSpanSetting);
//      if (this.hasDateRange && this.backendService.getPrefs('dateRanger.keepSelectionBetweenCharts').toUpperCase() === 'false'.toUpperCase()) {
//          if (!timeSpanSetting.selectedValue) {
//            timeSpanSetting.selectedValue = timeSpanSetting.defaultValue;
//          }
//        const period = convertTimeSpanToDuration(timeSpanSetting.selectedValue);
//        if (this.dateRange.period !== period) {
//          this.dateRange.period = period;
//          this.setPeriod();
//        }
//      }
      this.hasDateRange = (chart.defaultTimeSpan != null);
      if (this.hasDateRange && this.backendService.getPrefs('dateRanger.keepSelectionBetweenCharts').toUpperCase() === 'false'.toUpperCase()) {
        const period = convertTimeSpanToDuration(chart.defaultTimeSpan);
        if (this.dateRange.period !== period) {
          this.dateRange.period = period;
          this.setPeriod();
        }
      } else {
        // this.dateRange = null;
      }

    }
  }

  // TODO:Fix Use the constants here
  updateLabels(period: number) {
    this.dateLabels = [];
    const now = Date.now();
    switch (period) {
      case 1: {
                  for (let i = 240; i >= 1; i -= 10) {          // get the minutes for the past 4 hours
                    this.dateLabels.push(new DateLabels(new Date(now - i * 60000), 'h:MM'));
                  }
                  this.rangeMin = 18;
                  this.rangeMax = 24;
                  break; }
      case 15: {
                  for (let i = 1440; i >= 1; i -= 60) {          // get the 15 minutes for the past day
                    this.dateLabels.push(new DateLabels(new Date(now - i * 60000), 'h:MM'));
                  }
                  this.rangeMin = 18;
                  this.rangeMax = 24;
                  break; }
      case 60: {
                  for (let i = 168; i >= 1; i -= 7) {          // get the hourly data for the past week
                    this.dateLabels.push(new DateLabels(new Date(now - i * 3600000), 'mm/dd hh'));
                  }
                  this.rangeMin = 18;
                  this.rangeMax = 24;
                  break; }
      case 1440: {
                  for (let i = 30; i >= 1; i -= 1) {          // get the daily data for the past month
                    this.dateLabels.push(new DateLabels(new Date(now - i * 86400000), 'mm/dd'));
                  }
                  this.rangeMin = 20;
                  this.rangeMax = 30;
                  break; }
      default: {break; }
    }
    this.dateLabels.push(new DateLabels(new Date(), 'h:MM'));
  }

  ngAfterViewInit() {
    this.setup(this.cursorMin);
    this.setup(this.cursorMax);

    this.guide.nativeElement.style.width = (this.dateLabels.length * 30) + 2 + 'px';  // 30 is the width of block, replace the constant
    this.updateCursors = true;


    $(this.config.nativeElement).daterangepicker(this.options);
    $(this.config.nativeElement).on('apply.daterangepicker', {comp: this}, function(ev, picker) {
        ev.data.comp.onDateRangeUpdated({start: picker.startDate, end: picker.endDate});
    });
  }

  ngAfterViewChecked() {
    if (this.updateCursors) {
      this.moveCursorTo(this.cursorMin, this.rangeMin);
      this.moveCursorTo(this.cursorMax, this.rangeMax);
      this.highlightRange();
      this.updateCursors = false;
    }
  }

  setup(selCursor) {
    $(selCursor.nativeElement)
      .unbind('mousedown.dateranger')
      .bind('mousedown.dateranger', {comp: this}, function(e) {
          e.data.comp.clearEvents(e);
          $(document).unbind('mousemove.dateranger')
                     .bind('mousemove.dateranger', {comp: e.data.comp}, function(e) {e.data.comp.onMouseMove(e, selCursor); });

          $(this).addClass('cursorDrag');

          $(document).unbind('mouseup.dateranger')
                     .bind('mouseup.dateranger',  {comp: e.data.comp}, function(e) {e.data.comp.onMouseUp(e, selCursor); });
      });
  }

  onMouseMove(e, cursor) {
    this.clearEvents(e);
    let pos = Math.floor((e.pageX - $(this.guide.nativeElement).offset().left) / $(cursor.nativeElement).outerWidth());
    if (cursor === this.cursorMin) {
      if (pos < 0) { pos = 0; }
      if (pos >= this.rangeMax) { pos = this.rangeMax - 1; }
    }
    if (cursor === this.cursorMax) {
      if (pos < this.rangeMin) { pos = this.rangeMin + 1; }
      if (pos >= $(this.guide.nativeElement).children().length) { pos = $(this.guide.nativeElement).children().length; }
    }
    this.moveCursorTo(cursor, pos);
  }

  onMouseUp(e, cursor) {
    this.clearEvents(e);
    $(cursor.nativeElement).removeClass('cursorDrag');
    $(document).unbind('mousemove.dateranger mouseup.dateranger');

   if (cursor === this.cursorMin) {
      this.rangeMin = this.currPos;
    }
    if (cursor === this.cursorMax) {
      this.rangeMax = this.currPos;
    }
    this.highlightRange();
    // this.dateRange = new DateRange(this.period, this.dateLabels[this.rangeMin].dt, this.dateLabels[this.rangeMax].dt);
    this.dateRange.min = this.dateLabels[this.rangeMin].dt;
    this.dateRange.max = this.dateLabels[this.rangeMax].dt;
    this.eventService.rangeUpdated(this.dateRange);
  }

  moveCursorTo(selCursor, pos) {
    const dateRangerBlocks = $(this.dateRanger.nativeElement).children('.block');
    if (pos < 0 || pos >= (dateRangerBlocks.length - 0)) { return true; }
    if (pos === this.currPos) { return true; }

    // select slider hour block
    $(this.dateRanger.nativeElement).find('.blockSelected').removeClass('blockSelected');
    dateRangerBlocks.eq(pos).addClass('blockSelected');                     // blocks[pos].classList.add('blockSelected');
    $(this.dateRanger.nativeElement).find('.cursor').removeClass('cursorSelected');
    $(selCursor.nativeElement).addClass('cursorSelected');

    $(selCursor.nativeElement).css('margin-left', (pos * ($(selCursor.nativeElement).outerWidth() + 2)));
    this.currPos = pos;
  }

  highlightRange() {
//    const blocks = this.guide.nativeElement.children;
//    for (let i = 0; i < blocks.size; i++) {
//      if (i > this.maxPos && i < this.minPos) {
//        blocks[i].classList.add('highlighted');
//      } else {
//        blocks[i].classList.remove('highlighted');
//      }
//    }

    const guideBlocks = $(this.guide.nativeElement).children();
    guideBlocks.removeClass('highlighted');
    for (let i = this.rangeMin + 1; i < this.rangeMax; i++) {
      guideBlocks.eq(i).addClass('highlighted');
    }
  }

  clearEvents(e) {
    // prevent drag text selection of hour blocks
    $(document).selection && $(document).selection.empty();
    window.getSelection && window.getSelection().removeAllRanges();
    e.preventDefault && e.preventDefault();
    e.stopPropagation && e.stopPropagation();
  }


  onDateRangeUpdated(dateRange) {
    this.dateRange.min = dateRange.start.toDate();
    this.dateRange.max = dateRange.end.toDate();
    if (!this.showGranularity(this.constants['MIN' + this.dateRange.period])) {
      this.dateRange.period = this.calculateGranularity();
      this.adjustBoundries(this.dateRange);
    }
    this.updateSliderLabels();
    this.eventService.rangeUpdated(this.dateRange);
  }

  showGranularity(granularity) {
    // return moment(this.dateRange.min).isAfter(moment().subtract(granularity.AVAILABLE_RANGE, 'minutes'));
    return moment(this.dateRange.max).diff(moment(this.dateRange.min), 'minutes') <= granularity.AVAILABLE_RANGE;
  }

  calculateGranularity() {
    for (const gran of [1, 15, 60, 1440]) {
      if (this.showGranularity(this.constants['MIN' + gran])) { return gran; }
    }
    return 0;
  }

  setDefaultBoundries(period: number) {
    this.dateRange.max = new Date();
    const MS_PER_MINUTE = 60000;
    this.dateRange.min = new Date(this.dateRange.max.getTime() - this.constants['MIN' + period].AVAILABLE_RANGE * MS_PER_MINUTE);
  }

  adjustBoundries(range: DateRange) {
    const start = moment(range.min);
    const end = moment(range.max);

    switch (range.period) {
      case 1:    {start.startOf('minute'); end.endOf('minute'); break; }
      case 15:   {start.minutes(Math.floor(start.minutes() / 15) * 15); end.minutes(Math.ceil(end.minutes() / 15) * 15); break; }
      case 60:   {start.startOf('hour'); end.endOf('hour'); break; }
      case 1440: {start.startOf('day'); end.endOf('day'); break; }
      default:   break;
    }

/*
    if (range.period >= 1)    {range.min.setMilliseconds(0); range.min.setSeconds(0); range.max.setMilliseconds(0); range.max.setSeconds(0); range.max.setMinutes(range.max.getMinutes() + 1); }
    if (range.period >= 15)   {range.min.setMinutes(Math.floor(range.min.getMinutes() / 15) * 15); range.max.setMinutes(Math.ceil(range.min.getMinutes() / 15) * 15); }
    if (range.period >= 60)   {range.min.setMinutes(0); range.max.setMinutes(0); range.max.setHours(range.max.getHours() + 1); }
    if (range.period >= 1440) {range.min.setHours(0); range.max.setHours(0); range.max.setDate(range.max.getDate() + 1); }
 */
  }

  updateSliderLabels() {
    const sliderPoints = Math.floor((document.documentElement.clientWidth * .5) / (this.cursorMin.nativeElement.offsetWidth + 10));
    const step =  (this.dateRange.max.getTime() - this.dateRange.min.getTime()) / sliderPoints;
    // console.log('document.documentElement.clientWidth ' + (document.documentElement.clientWidth * 0.7) + '; this.cursorMin.nativeElement.clientWidth: ' + this.cursorMin.nativeElement.clientWidth + '; sliderPoints ' + sliderPoints);

    const format = 'mm/dd hh:MM';
/*    let format = 'h:MM';
    if (this.dateRange.period === 60) { format = 'mm/dd hh'; }
    if (this.dateRange.period === 1440) { format = 'mm/dd'; }
*/
    this.dateLabels = [];
    for (let i = 0; i < sliderPoints; i++) {
      const dt = new Date(this.dateRange.min.getTime() + i * step);
      const m = moment(dt);
      switch (this.dateRange.period) {
        case 1:    {m.startOf('minute'); break; }
        case 15:   {m.minutes(Math.floor(m.minutes() / 15) * 15); break; }
        case 60:   {m.startOf('hour'); break; }
        case 1440: {m.startOf('day'); break; }
        default:   break;
      }
      this.dateLabels.push(new DateLabels(m.toDate(), format)); // this.dateRange.period * 60000
    }
    this.rangeMin = 0;
    this.rangeMax = sliderPoints - 1;
    this.guide.nativeElement.style.width = (this.dateLabels.length * 30) + 2 + 'px';
    this.updateCursors = true;
  }
}

export class DateLabels {
  // ts: number;
  dt: Date;
  label: string;
  constructor(date: Date, format: string) {
    // this.ts = date.getTime() + date.getTimezoneOffset();
    this.dt = date;
    switch (format) {
      case 'h:MM': { this.label = date.getUTCHours() + ':' + date.getUTCMinutes(); break; }
      case 'mm/dd hh': { this.label = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + ' ' + date.getUTCHours(); break; }
      case 'mm/dd': { this.label = (date.getUTCMonth() + 1) + '/' + date.getUTCDate(); break; }
      default: { this.label = (date.getMonth() + 1) + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes(); break; }
    }
  }
}
