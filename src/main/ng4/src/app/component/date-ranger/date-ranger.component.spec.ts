import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateRangerComponent } from './date-ranger.component';

describe('DateRangerComponent', () => {
  let component: DateRangerComponent;
  let fixture: ComponentFixture<DateRangerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateRanger1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateRanger1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
