import { DateRangePickerModule } from '../date-range-picker/date-range-picker.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { DateRangerComponent } from './date-ranger.component';

@NgModule({
    imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    DateRangePickerModule
  ],
  declarations: [
    DateRangerComponent,
  ],
  providers: [],
  exports : [DateRangerComponent]
})
export class DateRangerModule { }
