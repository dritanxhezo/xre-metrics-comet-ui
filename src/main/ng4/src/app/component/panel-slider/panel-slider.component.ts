import { Chart } from '../../model/chart';
import { ChartData } from '../../model/chart-data';
import { ChartDataDef } from '../../model/chart-data-def';
import { TreeNode } from '../../model/tree-node';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';
import { Component, AfterViewInit, HostListener, Input, ElementRef, OnChanges, AfterViewChecked } from '@angular/core';
declare var $: any;

@Component({
  selector: 'panel-slider',
  templateUrl: './panel-slider.component.html',
  styleUrls: ['./panel-slider.component.css']
})
export class PanelSliderComponent implements AfterViewInit, OnChanges, AfterViewChecked {
  @Input() charts: Array<Chart>;
  @Input() selectedPageIndex = 0;

  editingItems = false;
  sparklineType = 'line';
  sparklineOptions: any;
  sparklineDatas = new Map<string, ChartData>();
  redrawSparklines = false;

  constructor(private el: ElementRef, private eventService: EventService, private backendService: BackendService) {
//    eventService.onSelectPage.subscribe(pageIndex => {
//      this.selectedChartIndex = pageIndex;
//    });
  }

  ngOnChanges() {
    // this.selectedPageIndex = 0;
    for (const chart of this.charts) {
      this.loadSparkline(chart);
    }
  }

  ngAfterViewChecked() {
    if (this.redrawSparklines) {
      for (const chart of this.charts) {
        if (this.sparklineDatas.get(chart.id)) {
          this.drawSparkline(chart, this.sparklineDatas.get(chart.id));
        }
      }
    }
  }

  ngAfterViewInit() {
    this.doResize();
    this.loadSparklineOptions();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.doResize();
  }

  doResize() {
    const container = $(this.el.nativeElement).find('.panelSliderContainer');
    const scrollclip = $(this.el.nativeElement).find('.scrollclip');
    const clipWidth = parseInt(container.innerWidth()) - 25;
    const clipHeight = parseInt(container.innerHeight()) - 10;
    scrollclip.css('clip', 'rect(0px,' + clipWidth + 'px, ' + clipHeight + 'px,0px)');

    const scrollstrip = $(this.el.nativeElement).find('.scrollstrip');   // use querySelector / querySelectorAll
    const stripWidth = scrollstrip.children().length * parseInt($('.item').outerWidth(true));
    scrollstrip.css('width', stripWidth);
  }

  doScroll(direction) {
    const moveDirection = (direction === 'left') ? 1 : -1;
    const container = $(this.el.nativeElement).find('.panelSliderContainer');
    const scrollstrip = $(this.el.nativeElement).find('.scrollstrip');

    const curPos = parseInt(scrollstrip.css('left'));
    const stripWidth = scrollstrip.children().length * parseInt($('.item').outerWidth(true));
    const scrollStep = parseInt(container.innerWidth());
    const delta = moveDirection * scrollStep;
    const newPos = curPos + scrollStep * moveDirection;

    if ((newPos > 0) || (newPos < stripWidth * -1)) {
    } else {
      scrollstrip.animate({ left: '+=' + delta }, 500);
    }
  }

  loadChart(chartIndex: number) {
    // this.selectedPageIndex = chartIndex;
    this.eventService.loadChart(this.charts[chartIndex]);
    // this.eventService.selectPage(chartIndex);
    this.redrawSparklines = true;
  }

  loadSparklineOptions() {
    this.backendService.getSparklineChartOptions(this.sparklineType)
      .subscribe(
      options => { this.sparklineOptions = options; this.redrawSparklines = true; },
      err => { console.log(err); });
  }

/*
  loadSparklineData(chart: Chart) {
    this.backendService.getChartData(chart.dataSources[0].dataSource, chart.settings)
      .subscribe(
      chartData => { this.sparklineDatas.set(chart.id, chartData); },  // this.drawChart();
      err => { console.log('Error getting chart: ' + err); });
  }
*/
  loadSparkline(chart: Chart) {
    let chartData: ChartData;
    //       this.backendService.getChartData(chart)
    //          .subscribe(
    //              cd => {chartData = cd },  // this.drawSparkline(chart, chartData);
    //              err => { console.log('Error getting chart: ' + err); });
    const min = 10, max = 100;
    const vals = new Array<number>();
    for (let i = 0; i < 25; i++) {
      vals.push(Math.floor(Math.random() * (max - min + 1) + min));
    }
    const cdd = [new ChartDataDef('sparkline', vals)];
    chartData = new ChartData(null, cdd, 'EST');
    this.sparklineDatas.set(chart.id, chartData);

    // this.drawSparkline(chart, chartData)
  }

  drawSparkline(chart: Chart, chartData: ChartData) {
    const values = chartData.data[0].values;
    const peityspan = $('#peity_' + chart.id);
    const peityChart = peityspan.peity(this.sparklineType, this.sparklineOptions);
    peityspan.text(values).change();
    this.redrawSparklines = false;
  }

  sliderType(typeOfSlider: string): boolean {
    const typeFromConfig: string = this.backendService.getPrefs('panelSlider.sliderType');
    if (typeFromConfig) {
      return typeFromConfig.toUpperCase() === typeOfSlider.toUpperCase();
    } else {
      return false;
    }
  }
}
