import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LoginService } from '../../service/login.service';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent {
  errorMessage: string;

  constructor(public dialogRef: MatDialogRef<AppComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private loginService: LoginService) { }

  doLogin(): void {
    this.loginService.login(this.data.login, this.data.password).subscribe(loginResponse => {
      this.data.parent.login(loginResponse);
      this.dialogRef.close();
    },
      err =>     {
      console.log(err);
      const status = err && err.status ? err.status : '';
      const statusText = err && err.statusText ? err.statusText : '';
      this.errorMessage = 'Unable to login:' +  status + ' ' + statusText;
    });
  }
}
