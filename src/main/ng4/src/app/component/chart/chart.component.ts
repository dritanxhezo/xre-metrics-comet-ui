import { Chart } from '../../model/chart';
import { Component, OnInit, Input, ElementRef, HostListener, AfterViewInit, ViewChild, AfterViewChecked } from '@angular/core';
import { ChartData } from '../../model/chart-data';
import { DateRange } from '../../model/date-range';
import { Filter } from '../../model/filter';
import { LegendItem } from '../../model/legend-item';
import { BaseChart } from '../../util/chart/base-chart';
import { ChartFactory } from '../../util/chart/chart-factory';
import { BackendService } from '../../service/backend.service';
import { EventService } from '../../service/event.service';
import { convertDateToUTC } from '../../util/util';
import { ChartColors } from '../../util/chart-colors';
import { Constants } from '../../util/constants';
import { openModal, closeModal, randomColor } from '../../util/util';
import { Observable } from 'rxjs';

declare var google: any;
declare var moment: any;

@Component({
  selector: 'chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']

})
export class ChartComponent implements OnInit, AfterViewInit, AfterViewChecked  {
  chart: Chart;
  chartData: ChartData;
  filters: Array<Filter>;
  chartAxes: any;
  chartHeightWidthRatio = .45;

  chartInstance: BaseChart;

  chartLoaded = false;
  viewInitialized = false;
  updateView = false;
  legendItems = new Array<LegendItem>();
  checkedLegendItemsValues: Array<string>;
  legendAllChecked = true;
  legendSortedBy = 'value';
  mergeMode = false;
  selectedLegendItems: Array<string>;

  @ViewChild('legendScroller') legendScroller: ElementRef;
  @ViewChild('legendContainer') legendContainer: ElementRef;
  @ViewChild('chartDiv') chartDiv: ElementRef;

  dateRange = new DateRange(1, moment().subtract(1, 'hours').toDate(), moment().toDate());

  constructor(private el: ElementRef, private backendService: BackendService, private eventService: EventService, private constants: Constants) {
    eventService.onChartSelected.subscribe(chart => {
      this.chart = chart;
      this.chartInstance = ChartFactory.createChart(chart.chartType, this);
    });
    eventService.onRangeUpdated.subscribe(dateRange => this.dateRange = dateRange);
    eventService.onUrlPathParamsChanged.subscribe(filters => this.filters = filters);
    eventService.onLoadChartData.subscribe(resetLegendSelection => this.loadChartData(resetLegendSelection));
  }

  ngOnInit() {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(() => {this.chartLoaded = true;
                                           this.drawChart();
                                          });
  }

  ngAfterViewInit() {
    this.viewInitialized = true;
    this.drawChart();
  }

  ngAfterViewChecked() {
    if (this.updateView) {
       this.drawChart();
       this.updateView = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.chartLoaded) {
      this.chartInstance.resizeChart();
      this.chartInstance.reDrawChart();
    }
  }

//  mergeFilters(filters: Array<Filter>) {
//     console.log('filters ' + JSON.stringify(filters));
//    if (this.chart && this.chart.settings) {
//      this.chart.settings.forEach(setting => {
//        // console.log(setting.selectedLabel + ':::' + setting.selectedValue + ':::' +  filters[setting.id]);
//        if (filters[setting.id]) {
//        // setting.selectedLabel = setting.selectedValue = filters[setting.id];
//            setting.selectedValue = filters[setting.id];
//        }
//      });
//    }
//  }

  loadChartData(resetLegendSelection: any) {
//    if (this.backendService.getPrefs('deepLinkEnabled').toUpperCase() === 'true'.toUpperCase()) {
//      if (this.filters) { this.mergeFilters(this.filters); this.filters = null; }
//    }
    this.checkedLegendItemsValues = resetLegendSelection ? null : this.checkedLegendItemsValues;
    this.mergeMode = false;
    this.legendAllChecked = true;
    if (this.constants.isABTestMode && this.constants.selectedPopolations && this.constants.selectedPopolations.length > 0) {
      this.chart.populations = this.constants.selectedPopolations;
    }
    const backendFunction = this.chart.settingModel === 'API_UI' ? this.backendService.getApiChartData : this.backendService.getChartData;
    const dateRange = this.chart.defaultTimeSpan ? this.dateRange : null;
    this.chart.dataSources.sort(function(ds1, ds2){ return ds1.axisIndex - ds2.axisIndex; });
    openModal();
    Observable.combineLatest(
        this.chart.dataSources.map(dataSourceDef => backendFunction(this.constants.isABTestMode ? dataSourceDef.populationsDataSource : dataSourceDef.dataSource, this.chart, dateRange, this.backendService, this.constants.isABTestMode))
      ).subscribe(chartDatas => {this.chartInstance.addChartSeries(chartDatas);
                                 this.flattenChartDataArray(chartDatas);
                                 if (! this.chartDiv) {
                                    this.updateView = true;  // draw the chart after the chart div element is available at AfterViewChecked cycle
                                 } else {
                                    this.drawChart();
                                 }
                                },
                  err => {
                            this.chartData = null;
                            closeModal();
                         },
                  () => {   closeModal(); }
                  );
  }

  flattenChartDataArray(chartDatas: Array<ChartData>) {
    this.chartData = null;
    chartDatas.forEach(chd => {this.chartData = this.concatChartData(this.chartData, chd); });
  }

  concatChartData(chartData1: ChartData, chartData2: ChartData) {
    if (chartData1) {
      const dataObj = chartData1.data ? chartData1.data : {};
      chartData1.data = Object.assign(dataObj, chartData2.data);
    } else {
      chartData1 = chartData2;
    }
    return chartData1;
  }

  drawChart() {
    if (this.viewInitialized && this.chartLoaded && this.chartDiv) {
      this.initChart();
      this.chartInstance.setChartColors(null);
      const legend = this.chartInstance.createLegendEntries(this.chartData);
      let linesShown = null;
      if (this.checkedLegendItemsValues && this.checkedLegendItemsValues.length > 0) {
        legend.forEach(item => item.isChecked = this.checkedLegendItemsValues.indexOf(this.chartInstance.extractLegendValue(item.value)) > -1);
        linesShown = legend.filter(item => item.isChecked).map(item => parseInt(item.key) + 1);
      }
      this.legendItems = legend;
      this.chartInstance.sortComponentLegend(this.legendSortedBy);
      this.chartInstance.generateDataTable(this.chartData);
      this.chartInstance.setChartOptionTimeZone(this.chartData.timeZone);
      this.chartInstance.addSeriesOptions();
      if (linesShown != null) {
        this.chartInstance.setChartColors(linesShown);
        this.chartInstance.setChartElements(linesShown);
      }
      this.chartInstance.resizeChart();
      this.chartInstance.reDrawChart();
    }
  }

  initChart() {
    if (this.viewInitialized) {
      this.chartInstance.initChart();
    }
  }

  onLegendChanged() {
      const checkedItems = this.legendItems.filter(keyVal => keyVal.isChecked);
      const linesShown = checkedItems.map(keyVal => parseInt(keyVal.key) + 1);
      this.checkedLegendItemsValues = checkedItems.map(item => this.chartInstance.extractLegendValue(item.value));
      this.chartInstance.setChartColors(linesShown);
      this.chartInstance.setChartElements(linesShown);
      this.chartInstance.resizeChart();
      this.chartInstance.reDrawChart();
  }

  onLegendChangeAll() {
    this.legendItems.forEach(entry => entry.isChecked = this.legendAllChecked);
    this.onLegendChanged();
  }

  sortLegend(sortBy: string) {
    switch (sortBy) {
      case 'alpha': { this.legendItems.sort(this.alphaComp); break; }
      case 'value': { this.legendItems.sort(this.valueComp); break; }
      case 'movers': { this.legendItems.sort(this.moverComp); break; }
      default: {break; }
    }
    this.legendSortedBy = sortBy;
  }

  alphaComp(a: LegendItem, b: LegendItem) {
    return a.value.localeCompare(b.value);
  }

  valueComp(a: LegendItem, b: LegendItem) {
    return Math.sign(b.avgValue - a.avgValue);
  }

  moverComp(a: LegendItem, b: LegendItem) {
    return Math.sign(b.stdDeviation - a.stdDeviation);
  }

  showLegendValues(): boolean {
    return (this.backendService.getPrefs('legend.showValues').toUpperCase() === 'true'.toUpperCase());
  }
  onMergeModeChanged() {
    this.onLegendChanged();
  }
  showLegendHeader() {
    return !this.constants.isABTestMode && (!this.chartInstance || this.chartInstance.showLegendHeader);
  }

  showMerge() {
    return !this.chartInstance || this.chartInstance.showMerge;
  }
}
