import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { DateRangePickerComponent } from './date-range-picker.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [CommonModule, BrowserModule, FormsModule],
  declarations: [DateRangePickerComponent],
  exports: [DateRangePickerComponent]
})

export class DateRangePickerModule { }
