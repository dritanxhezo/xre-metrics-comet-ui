import { DateRange } from '../../model/date-range';
import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
declare var $: any;
declare var moment: any;

@Component({
  selector: 'date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.css']
})
export class DateRangePickerComponent implements OnInit, OnChanges {

//  @Input() controlType;
  @Input() parentOptions;
  @Output() rangeUpdated = new EventEmitter();

  private dateFormat = 'MM/DD/YYYY HH:mm:ss';
//  private startDate = moment().subtract(6, 'hours').calendar();
//  private endDate = moment().calendar();
//  private minDate = moment().subtract(30, 'days').calendar();
//  private maxDate = moment().calendar();
  private dateLabel: string;

  @ViewChild('dateRangePicker') dateRangePicker: ElementRef;

  private options = {
    'showDropdowns': true,
    'timePickerIncrement': 5,
    'timePicker': true,
    'timePicker24Hour': true,
    'autoApply': true,
    'autoUpdateInput': true,
    'opens': 'right',
    'drops': 'down',
    'buttonClasses': 'btn btn-sm',
    'applyClass': 'btn-default',
    'cancelClass': 'btn-default',
    'startDate': moment().utc().subtract(12, 'hours'),
    'endDate': moment().utc(),
    'minDate': moment().utc().subtract(29, 'days'),
    'maxDate': moment().utc(),
    'ranges': {
       'Last 12hr': [moment().subtract(12, 'hours'), moment()],
       'Last Primetime': [moment().utc().startOf('day').subtract(5, 'hours'), moment().startOf('day').add(2, 'hours')],
       'Last Reboot':  [moment().utc().startOf('day').add(2, 'hours'), moment().utc().startOf('day').add(4, 'hours')],
       'Today': [moment().utc().startOf('day'), moment()],
       'Yesterday': [moment().utc().subtract(1, 'days').startOf('day'), moment().utc().subtract(1, 'days').endOf('day')],
       'Last 7 Days': [moment().utc().subtract(6, 'days'), moment().utc()],
       'Last 30 Days': [moment().utc().subtract(29, 'days'), moment().utc()],
       'This Month': [moment().utc().startOf('month'), moment().utc().endOf('month')],
       'Last Month': [moment().utc().subtract(1, 'month').startOf('month'), moment().utc().subtract(1, 'month').endOf('month')]
    }
  };

//  constructor(private dateRangePicker: ElementRef) {
//  }

  ngOnInit() {
    Object.assign(this.options, this.parentOptions);
    $(this.dateRangePicker.nativeElement).daterangepicker(this.options);

//    $(this.dateRangePicker.nativeElement).data('daterangepicker').setStartDate(moment().subtract(12, 'days'));
//    $(this.dateRangePicker.nativeElement).data('daterangepicker').setEndDate(moment());
    // this.selected = new DateRange(0, this.startDate.toDate(), this.endDate.toDate());
    $(this.dateRangePicker.nativeElement).on('apply.daterangepicker', {comp: this}, function(ev, picker) {
//        ev.data.comp.selected.min = picker.startDate.toDate();
//        ev.data.comp.selected.max = picker.endDate.toDate();
        ev.data.comp.dateLabel = picker.startDate.format(ev.data.comp.dateFormat) + ' - ' + picker.endDate.format(ev.data.comp.dateFormat);
//        ev.data.comp.showGranularity(picker.startDate);
//        ev.data.comp.rangeUpdated.emit(new DateRange(0, picker.startDate.toDate(), picker.endDate.toDate()));
        ev.data.comp.rangeUpdated.emit({start: picker.startDate, end: picker.endDate});
    });
//    this.dateLabel = this.startDate.format(this.dateFormat) + ' - ' + this.endDate.format(this.dateFormat);
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes.parentOptions) {
      Object.assign(this.options, this.parentOptions);
      $(this.dateRangePicker.nativeElement).daterangepicker(this.options);
//      if (this.parentOptions.startDate && this.parentOptions.endDate) {
        this.dateLabel = this.parentOptions.startDate.format(this.dateFormat) + ' - ' + this.parentOptions.endDate.format(this.dateFormat);
//      }
    }
  }
}
