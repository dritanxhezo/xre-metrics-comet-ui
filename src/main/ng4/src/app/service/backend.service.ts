import { Chart } from '../model/chart';
import { ChartData } from '../model/chart-data';
import { DateRange } from '../model/date-range';
import { Filter } from '../model/filter';
import { KeyValue } from '../model/key-value';
import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Page } from '../model/page';
import { Setting } from '../model/setting';
import { Population } from '../model/population';
import { TreeNode } from '../model/tree-node';
import { Favorite } from '../model/favorite';
import { convertDateToTimeStamp, convertDurationToTimeSpan, convertDurationForOldCharts } from '../util/util';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class BackendService {
  private static hostUrl = '/proxy/';
  private static timeOut = 60000;  // timeout after a minute
  favorites = this.getFavorites();

  constructor(private http: Http) { }

  getVersion(): Observable<string> {
    return this.http.get(BackendService.hostUrl + 'version')
      .map((res: Response) => res.text() )
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getReports(): Observable<Array<TreeNode>> {
     return this.http.get(BackendService.hostUrl + '/api/ui/report/reports-tree')
       .map((res: Response) => res.json())
       .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

   // return this.http.get('./data/reports-tree.json').map(res => res.json());
   // return this.http.get('./data/reports-tree-ABTesting.json').map(res => res.json());
  }

  getChartData(dataSource: string, chart: Chart, dateRange: DateRange, service, isABTestMode: boolean): Observable<ChartData> {
    if (chart && chart.settings) {
      for (const setting of chart.settings) {
        dataSource = dataSource + (dataSource.includes('?') ? '&' : '?') + setting.uiGroup + '=' + setting.selectedValue;   // setting.defaultValue
      }
    }

    if (dateRange) {
      // dataSource = dataSource + (dataSource.includes('?') ? '&' : '?') + 'timeSpan=' + convertDurationToTimeSpan(dateRange.period) + '&startTime=' + dateRange.min.getTime() + '&endTime=' + dateRange.max.getTime();
        document.cookie = 'overridePointCountFlag=' + convertDurationForOldCharts(dateRange.max.getTime() - dateRange.min.getTime()) + ';path=/';
    }

    // replace all the || with a comma, so that we don't have to use encodeURIComponent;
    dataSource = dataSource.replace(/\|\|/g, ',');

    console.log('Retrieving data from datasource: ' + BackendService.hostUrl + dataSource);  //  + '; dateRange: ' + JSON.stringify(dateRange));
    return service.http.get(BackendService.hostUrl + dataSource)
      .timeout(BackendService.timeOut)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getSettingValues(dataSource: string): Observable<Map<string, string>> {
    // replace all the || with a comma, so that we don't have to use encodeURIComponent;
    dataSource = dataSource.replace(/\|\|/g, ',');
    return this.http.get(BackendService.hostUrl + dataSource)
      .timeout(BackendService.timeOut)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getApiChartData(dataSource: string, chart: Chart, dateRange: DateRange, service, isABTestMode: boolean): Observable<ChartData> {
    let filters = {};
    if (isABTestMode && chart && chart.populations) {
      filters['populations'] = chart.populations;
    }
    const settings = {};
    if (chart && chart.settings) {
       for (const setting of chart.settings) {
         if (setting.selectedValue) { settings[setting.id] = setting.selectedValue; }
     }
    }
    if (dateRange) {  //  && settings.find(setting => setting.id === 'timeSpan')) {
      settings['timeSpan'] = convertDurationToTimeSpan(dateRange.period);
      dataSource = dataSource + (dataSource.includes('?') ? '&' : '?') + 'startTime=' + dateRange.min.getTime() + '&endTime=' + dateRange.max.getTime();
    }

    if (isABTestMode) {
      filters['commonParams'] = settings;
    } else {
      filters = settings;
    }

    console.log('Retrieving data from datasource: ' + BackendService.hostUrl + dataSource + '; filters: ' + JSON.stringify(filters) + '; dateRange: ' + JSON.stringify(dateRange));
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Cache-control', 'no-cache');
    headers.append('Cache-control', 'no-store');
    headers.append('Expires', '0');
    headers.append('Pragma', 'no-cache');
    return service.http.post(BackendService.hostUrl + dataSource, JSON.stringify(filters), {headers: headers})
      .timeout(BackendService.timeOut)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getApiSettingValues(chartId: string, settings, settingId, dateRange: DateRange): Observable<Array<Setting>> {
    let settingsRequestPath = BackendService.hostUrl + '/api/ui/settings/get/' + chartId;
    if (settingId !== undefined && settingId !== null) {
        settingsRequestPath += '?settingId=' + settingId;
    }
    const filters = {};
    if (settings) {
      for (const setting of settings) {
        if (setting.selectedValue) { filters[setting.id] = setting.selectedValue; }
      }
    }

    if (dateRange) {
      filters['timeSpan'] = convertDurationToTimeSpan(dateRange.period);
      // settingsRequestPath = settingsRequestPath + (settingsRequestPath.includes('?') ? '&' : '?') + 'startTime=' + convertDateToTimeStamp(dateRange.min) + '&endTime=' + convertDateToTimeStamp(dateRange.max);
      settingsRequestPath = settingsRequestPath + (settingsRequestPath.includes('?') ? '&' : '?') + 'startTime=' + dateRange.min.getTime() + '&endTime=' + dateRange.max.getTime();
    }

    console.log('Retrieving data from datasource: ' + settingsRequestPath + '; filters: ' + JSON.stringify(filters) + '; dateRange: ' + JSON.stringify(dateRange));
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Cache-control', 'no-cache');
    headers.append('Cache-control', 'no-store');
    headers.append('Expires', '0');
    headers.append('Pragma', 'no-cache');
    return this.http.post(settingsRequestPath, JSON.stringify(filters), {headers: headers})
            .timeout(BackendService.timeOut)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getTableValues(dataSource: string): Observable<any> {
    return this.http.get(BackendService.hostUrl + dataSource)
      .timeout(BackendService.timeOut)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getChartDefaultOptions(chartType: string) {
    return this.http.get('./data/' + chartType + '-default-options.json').map(res => res.json());
  }

  getSparklineChartOptions(chartType: string) {
    return this.http.get('./data/' + chartType + '-chart-sparkline-options.json').map(res => res.json());
  }

  getChartColors() {
    return this.http.get('./data/chart-colors.json').map(res => res.json());
  }

  getPredefinedPopulations() {
    return this.http.get('./data/predefined-populations.json').map(res => res.json());
  }
  getFavorites(): Array<Favorite> {
    return JSON.parse(localStorage['favorites'] || '[]');
  }

  addFavorite(favorite: Favorite): Array<Favorite> {
    this.favorites.unshift(favorite);
    localStorage['favorites'] = JSON.stringify(this.favorites);
    return this.favorites;
  }

  removeFavorite(favorite: Favorite): Array<Favorite> {
    const favIndex = this.getFavoriteIndex(favorite.id);
    if (favIndex !== -1 ) {
      this.favorites.splice(favIndex, 1 );
      localStorage['favorites'] = JSON.stringify(this.favorites);
    }
    return this.favorites;
  }

  getFavoriteIndex(favId: string): number {
    if (this.favorites.length < 1) {
      return -1;
    } else {
      return this.favorites.findIndex(fav => fav.id === favId);
    }
  }

  getPrefs(configName: string) {
    if (!localStorage['dateRanger.keepSelectionBetweenCharts']) {
      // keepSelectionBetweenCharts: When you have the time ranger set on a particular range, and you load another chart,
      // the range is applied to the new chart; If this setting is false, the default range specified in the settings of
      // the new chart would be applied.
      localStorage['dateRanger.keepSelectionBetweenCharts'] = true;
    }
    if (!localStorage['dateRanger.rangerType']) {     // calendar | slider
      localStorage['dateRanger.rangerType'] = 'calendar';
    }
    if (!localStorage['legend.showValues']) {
      localStorage['legend.showValues'] = true;
    }
    if (!localStorage['panelSlider.sliderType']) {    // tabs | slides
      localStorage['panelSlider.sliderType'] = 'tabs';
    }
    if (!localStorage['deepLinkEnabled']) {
      localStorage['deepLinkEnabled'] = false;
    }
    return localStorage[configName];
  }
}
