import { Chart } from '../model/chart';
import { ChartData } from '../model/chart-data';
import { DateRange } from '../model/date-range';
import { Filter } from '../model/filter';
import { PageSection } from '../model/page-section';
import { TreeNode } from '../model/tree-node';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class EventService {
  // Observable string sources
  private selectReportSource = new Subject<TreeNode>();
  private urlPathSource = new Subject<string>();
  private urlPathParamsSource = new Subject<Array<Filter>>();
  private closeSlideoutSource = new Subject();
  private chartSelectedSource = new Subject<Chart>();
  private loadChartDataSource = new Subject();
  private loadChartFiltersSource = new Subject();
  private appliedFiltersSource = new Subject();
  private loadTableDataSource = new Subject<Array<PageSection>>();
  private rangeUpdatedSource = new Subject<DateRange>();
  private loadDashboardSource = new Subject<string>();
  private unLoadDashboardSource = new Subject();

  // Observable string streams
  onSelectReport = this.selectReportSource.asObservable();
  onUrlPathChanged = this.urlPathSource.asObservable();
  onUrlPathParamsChanged = this.urlPathParamsSource.asObservable();
  onCloseSlideout = this.closeSlideoutSource.asObservable();
  onChartSelected = this.chartSelectedSource.asObservable();
  onLoadChartData = this.loadChartDataSource.asObservable();
  onLoadChartFilters = this.loadChartFiltersSource.asObservable();
  onAppliedFilters = this.appliedFiltersSource.asObservable();
  onLoadTableData = this.loadTableDataSource.asObservable();
  onRangeUpdated = this.rangeUpdatedSource.asObservable();
  onLoadDashboard = this.loadDashboardSource.asObservable();
  onUnLoadDashboard = this.unLoadDashboardSource.asObservable();

  constructor() { }

  selectReport(report: TreeNode) {
    this.selectReportSource.next(report);
  }
  selectReportByName(urlPath: string) {
    this.urlPathSource.next(urlPath);
  }
  setReportFiltersByUrl(filters: Array<Filter>) {
    this.urlPathParamsSource.next(filters);
  }
  setUrlFromFilter() {
    this.appliedFiltersSource.next();
  }
  closeSlideout() {
    this.closeSlideoutSource.next();
  }
  chartSelected(chart: Chart) {
    this.chartSelectedSource.next(chart);
  }
  loadChartData(resetLegendSelection: boolean) {
    this.loadChartDataSource.next(resetLegendSelection);
    // this.appliedFiltersSource.next();
  }
  loadChartFilters() {
    this.loadChartFiltersSource.next();
  }
  loadChart(chart: Chart) {
    this.chartSelected(chart);
    this.loadChartData(true);
    this.loadChartFilters();
//    this.loadChartFiltersSource.next();
  }
  loadTables(pageSections: Array<PageSection>) {
    this.loadTableDataSource.next(pageSections);
  }
  rangeUpdated(dateRange: DateRange) {
    this.rangeUpdatedSource.next(dateRange);
    this.loadChartData(false);
    this.loadChartFilters();
  }
  loadDashboard(dashboardUrl: string) {
    this.loadDashboardSource.next(dashboardUrl);
  }
  unLoadDashboard() {
    this.unLoadDashboardSource.next();
  }
  autoRefresh() {
    this.loadChartData(false);
    this.loadChartFilters();
  }
}
