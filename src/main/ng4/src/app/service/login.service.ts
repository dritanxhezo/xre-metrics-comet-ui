import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
  private static hostUrl = '/security/';
  private static timeOut = 60000;  // timeout after a minute

  constructor(private http: Http) { }

  login(login: string, password: string): Observable<Response> {
    const headers = new Headers();
    headers.append('authorization', 'Basic ' + btoa(login + ':' + password));
    headers.append('X-Requested-With', 'XMLHttpRequest');
    return this.http.post(LoginService.hostUrl + 'user', {}, {headers: headers})
      .map((res: Response) => res )
      .catch((error: any) => Observable.throw(error));
  }

  logout(): Observable<Response> {
    const headers = new Headers();
    headers.append('X-Requested-With', 'XMLHttpRequest');
    return this.http.post(LoginService.hostUrl + 'logout', {}, {headers: headers})
      .map((res: Response) => res )
      .catch((error: any) => Observable.throw(error));
  }
}
