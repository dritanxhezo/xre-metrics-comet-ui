export const ChartColors: Array<string> =  [
'#2196f3',
'#ff5722',
'#4caf50',
'#7e57c2',
'#29b6f6',
'#8bc34a',
'#ffca28',
'#8d6e63',
'#ec407a',
'#757575',
'#607d8b',
'#3f51b5',
'#6a1b9a',
'#0d47a1',
'#26c6da',
'#d4e157',
'#fb8c00',
'#80deea',
'#f44336',
'#00695c',
'#f9a825',
'#6d4c41',
'#78909c',
'#b71c1c',
'#424242',
'#1a237e',
'#006064',
'#00b8d4',
'#64dd17',
'#ffcc80',
'#795548',
'#7c4dff',
'#40c4ff',
'#ffe57f',
'#37474f',
'#e57373',
'#2979ff',
'#00bfa5',
'#ff6e40',
'#546e7a',
'#ad1457',
'#558b2f',
'#3949ab',
'#64b5f6',
'#01579b',
'#ff8f00',
'#009688',
'#4e342e',
'#455a64',
'#ff6f00',
'#afb42b',
'#00acc1',
'#4527a0',
'#42a5f5',
'#26a69a',
'#ff80ab',
'#43a047',
'#ffeb3b',
'#9ccc65',
'#ffa000',
'#bf360c',
'#a1887f',
'#263238',
'#ef5350',
'#00796b',
'#e64a19',
'#212121',
'#880e4f',
'#aed581',
'#0097a7',
'#1565c0',
'#ab47bc',
'#5c6bc0',
'#66bb6a',
'#fbc02d',
'#e53935',
'#283593',
'#00838f',
'#7cb342',
'#ba68c8',
'#827717',
'#48c9b0',
'#1b4f72',
'#a569bd',
'#7d6608',
'#e74c3c',
'#5e35b1',
'#7b7d7d',
'#16a085',
'#4d5656',
'#2ecc71',
'#512e5f',
'#273746',
'#6e2c00',
'#a6acaf',
'#145a32',
'#af601a',
'#1b2631',
'#ef9a9a',
'#c2185b',
'#e1bee7',
'#6200ea',
'#2962ff',
'#00e5ff',
'#81c784',
'#ffab00',
'#dd2c00',
'#3e2723',
'#90a4ae',
'#c0392b',
'#1abc9c',
'#5dade2',
'#8e44ad',
'#f4d03f',
'#b03a2e',
'#5499c7',
'#b9770e',
'#95a5a6',
'#186a3b',
'#d7bde2',
'#85929e',
'#212f3d',
'#c0ca33',
'#edbb99',
'#909497',
'#a9dfbf',
'#ca6f1e',
'#7b241c',
'#d50000',
'#f48fb1',
'#d500f9',
'#9575cd',
'#3d5afe',
'#0277bd',
'#4dd0e1',
'#b9f6ca',
'#82b1ff',
'#689f38',
'#ffff00',
'#ffb300',
'#d84315',
'#a7ffeb',
'#e65100',
'#d7ccc8',
'#bdbdbd',
'#148f77',
'#3498db',
'#5b2c6f',
'#fcf3cf',
'#ec7063',
'#73c6b6',
'#a9cce3',
'#f5b041',
'#d5dbdb',
'#82e0aa',
'#9b59b6',
'#17202a',
'#9e9d24',
'#dc7633',
'#cacfd2',
'#27ae60',
'#f0b27a',
'#d98880',
'#fce4ec',
'#ea80fc',
'#b39ddb',
'#c5cae9',
'#0288d1',
'#18ffff',
'#69f0ae',
'#ccff90',
'#bbdefb',
'#ffc400',
'#ffab91',
'#64ffda',
'#e0e0e0',
'#ffd180',
'#bcaaa4',
'#0e6251',
'#d6eaf8',
'#7d3c98',
'#eceff1',
'#f7dc6f',
'#cb4335',
'#0e6655',
'#d4e6f1',
'#ecf0f1',
'#28b463',
'#ebdef0',
'#bfc9ca',
'#c6ff00',
'#566573',
'#e59866',
'#784212',
'#34495e',
'#f2d7d5',
'#a3e4d7',
'#2874a6',
'#d2b4de',
'#b7950b',
'#f5b7b1',
'#b3b6b7',
'#a2d9ce',
'#1f618d',
'#fad7a0',
'#717d7e',
'#abebc6',
'#76448a',
'#abb2b9',
'#a04000',
'#e5e7e9',
'#1e8449',
'#f5cba7',
'#283747',
'#d1f2eb',
'#21618c',
'#e8daef',
'#9a7d0a',
'#fadbd8',
'#979a9a',
'#d0ece7',
'#1a5276',
'#fdebd0',
'#5f6a6a',
'#d5f5e3',
'#633974',
'#d5d8dc',
'#fff59d',
'#873600',
'#196f3d',
'#fae5d3',
'#00c853',
'#212f3c',
'#641e16',
'#fdd835',
'#ff9800',
'#651fff'
];
