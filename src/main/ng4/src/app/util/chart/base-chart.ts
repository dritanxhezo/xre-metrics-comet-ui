import { ChartComponent } from '../../component/chart/chart.component';
import { ChartData } from '../../model/chart-data';
import { convertDateToUTC, average, standardDeviation, randomColor } from '../../util/util';
import { ChartColors } from '../../util/chart-colors';
import { LegendItem } from '../../model/legend-item';
import { Chart } from '../../model/chart';

declare var google: any;
declare var moment: any;

export abstract class BaseChart {
  type: string;
  googleChart: any;
  chartComponent: any;
  showLegendHeader = true;
  showMerge = true;

  dataView: any;
  currentChartOptions: string;
  chartOptions: any;
  chartSeries: any;
  chartHeightWidthRatio = .45;
  colMd10Width = .83;
  colMd2Width = .16;

  constructor(type: string, chartComponent: any) {
    this.type = type;
    this.chartComponent = chartComponent;
    // load the chart default options
    this.loadDefaultChartOptions(this.type);
  }

  initChart() {
    this.googleChart = this.createGoogleChart();
    this.addListener();
    this.chartOptions =  JSON.parse(this.currentChartOptions);
    this.applyOptions();
  }

  abstract createGoogleChart();

  addListener() {
    google.visualization.events.addListener(this.googleChart, 'onmouseover', event => {
      this.selectLegend(event);
    });
    google.visualization.events.addListener(this.googleChart, 'onmouseout', event => {
      this.unselectLegend(event);
    });
  }

  selectLegend(event: any) {
    if (!this.chartComponent.mergeMode) {
      const label = this.getElementLegendLabel(event);
      const key = this.findLegendIndex(label);
      const elementId = 'legendItem_' + key;
      const element = document.getElementById(elementId);
      element.classList.add('chart-legend-label-selected');
    }
  }

  unselectLegend(event: any) {
    if (!this.chartComponent.mergeMode) {
      const label = this.getElementLegendLabel(event);
      const key = this.findLegendIndex(label);
      const elementId = 'legendItem_' + key;
      const element = document.getElementById(elementId);
      element.classList.remove('chart-legend-label-selected');
    }
  }

  getElementLegendLabel(event: any) {
    return this.dataView.getColumnLabel(event.column);
  }

  selectElement(legendValue: string) {
    if (!this.chartComponent.mergeMode) {
      const index = this.findIndex(legendValue);
      if (index != null) {
        this.googleChart.setSelection(this.getSelection(index));
      }
    }
  }

  unselectElement(legendValue: string) {
    if (!this.chartComponent.mergeMode) {
      const index = this.findIndex(legendValue);
      if (index != null) {
        this.googleChart.setSelection([]);
      }
    }
  }

  findIndex(legendValue: string) {
      for (let i = 1; i < this.dataView.getNumberOfColumns(); i++) {
        const value = this.dataView.getColumnLabel(i);
        if (value === this.extractLegendValue(legendValue)) {
          return i;
        }
      }
    return null;
  }

  findLegendIndex(legendValue: string) {
    return this.chartComponent.legendItems.find(item => this.extractLegendValue(item.value) === legendValue).key;
  }

  getSelection(index: number): any {
    return [{column: index}];
  }

  reDrawChart() {
    if (this.dataView.getNumberOfRows() > 0) {
      this.googleChart.draw(this.dataView, this.chartOptions);
    }
  }

  generateDataTable(chartData: ChartData) {
    const dataTable = new google.visualization.DataTable();
    const date_formatter = new google.visualization.DateFormat({pattern: 'M/d/yy H:mm'});
    dataTable.addColumn('datetime', 'X');

    Object.keys(chartData.data).forEach(key => dataTable.addColumn('number', key));

    for (let i = 0; i < chartData.legend.length; i++) {
      const arr = [];
      arr.push(convertDateToUTC(new Date(chartData.legend[i])));
      Object.keys(chartData.data).forEach(key => {
        let value = chartData.data[key][i];
        switch (value) {
          case 'Infinity':
          case 'NaN': {
            value = null;
          }
        }
        arr.push(this.getDatatableValue(value));
      });
      dataTable.addRow(arr);
    }
    date_formatter.format(dataTable, 0);
      const timeAxis = {viewWindow: {min: moment(convertDateToUTC(new Date(chartData.legend[0]))).subtract(1, 'minutes').toDate(),
                                     max: moment(convertDateToUTC(new Date(chartData.legend[chartData.legend.length - 1]))).add(1, 'minutes').toDate()}};
    Object.assign(this.chartOptions.hAxis, timeAxis);

    this.dataView = new google.visualization.DataView(dataTable);
  }

  getDatatableValue(value: any) {
    return value;
  }

  setChartOptionTimeZone(timeZone: string) {
    if (this.chartOptions && this.chartOptions.hAxis && this.chartOptions.hAxis.title && this.chartOptions.hAxis.title === 'Date/Time') {
      this.chartOptions.hAxis.title += ' (' + timeZone + ')';
    }
  }

  setChartElements(lines: Array<number>) {
      lines.unshift(0);       // preserve the first column (axis X data)
      if (this.chartComponent.mergeMode) {
         this.dataView.setColumns([0, this.getMergeColumn(this.chartComponent.chart.mergeFunction, lines)]);
      } else {
        if (lines.length === 1) {
          const columns = [0, {
            type: 'number',
            label: '',
            calc: (dataTable: any, rowNum: number): number => {
              return null;
            }
          }];
          this.dataView.setColumns(columns);
        } else {
          this.dataView.setColumns(lines);
        }
      }
  }

  getMergeColumn(mergeFunction: string, lines: Array<number>) {
    switch (mergeFunction) {
      case 'SUM': {
          return {
            type: 'number',
            label: 'Sum',
            calc: (dataTable: any, rowNum: number): number => {
              let sum = 0;
              lines.forEach(line => {
                if (line > 0) {
                  const value = dataTable.getValue(rowNum, line);
                  if (value != null) {
                    sum += value;
                  }
                }
              });
              return sum;
            }
          };
       }
      default: {
        return {
            type: 'number',
            label: 'Average',
            calc: (dataTable: any, rowNum: number): number => {
              let average = 0;
              lines.forEach(line => {
                if (line > 0) {
                  const value = dataTable.getValue(rowNum, line);
                  if (value != null) {
                    average += value;
                  }
                }
              });
              return average / (lines.length - 1);
            }
          };
      }
    }
  }

  loadDefaultChartOptions(chartType) {
    this.chartComponent.backendService.getChartDefaultOptions(chartType)
                        .subscribe( options => { this.currentChartOptions = JSON.stringify(options); },
                                        err => { console.log(err); });
  }

  applyOptions() {
    Object.keys(this.chartComponent.chart.options).forEach(
                                      opt => this.chartOptions[opt] ?
                                                Object.assign(this.chartOptions[opt], JSON.parse(this.chartComponent.chart.options[opt])) :
                                                this.chartOptions[opt] = JSON.parse(this.chartComponent.chart.options[opt]));
  }

  resizeChart() {
    this.chartOptions.width = (this.chartComponent.chartDiv.nativeElement.getBoundingClientRect().width === 0) ?
                                  (window.innerWidth * this.colMd10Width) :
                                  this.chartComponent.chartDiv.nativeElement.getBoundingClientRect().width;
    this.chartOptions.height = this.chartOptions.width * this.chartHeightWidthRatio;

    if (this.chartComponent.legendContainer) {
       this.chartComponent.legendContainer.nativeElement.style.height = this.chartOptions.height + 'px';
    }
  }

  setChartColors(lines: Array<number>) {
    if (lines && lines.length > 0) {
      this.chartOptions.colors = lines.map(i => ChartColors[i - 1]);
      if (this.chartComponent.mergeMode) {
        this.chartOptions.colors.unshift(ChartColors[this.chartComponent.legendItems.length]);
      }
    } else {
      if (this.chartComponent.chartData && this.chartComponent.chartData.data) {
         const dataLen = Object.keys(this.chartComponent.chartData.data).length;
         while (dataLen > ChartColors.length) {
              ChartColors.push(randomColor());
         }
      }
      this.chartOptions.colors = ChartColors;
    }
  }

  addChartSeries(chartDatas: Array<ChartData>) {
    this.chartSeries = {};
    let seriesIndex = 0;
    for (let i = 0; i < chartDatas.length; i++) {
       Object.keys(chartDatas[i].data).forEach(k => {
         const index = seriesIndex++;
         this.chartSeries[index] = {targetAxisIndex: i};
         this.updateChartSeriesElement(index);
       });
    }
  }

  addSeriesOptions() {
    this.chartOptions.series = this.chartSeries;
  }

  updateChartSeriesElement(index: number) {}

  sortComponentLegend(sortBy: string) {
    this.chartComponent.sortLegend(sortBy);
  }

  createLegendEntries(chartData: ChartData) {
    return Object.keys(chartData.data).map(this.toLegendEntry, chartData.data);
  }

  toLegendEntry(el, index, arr) {
    return new LegendItem(index.toString(), el, true, ChartColors[index], average(this[el]), standardDeviation(this[el]));
  }

  extractLegendValue(legendValue: string) {
    return legendValue;
  }

}
