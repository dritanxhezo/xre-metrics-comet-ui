import { BaseChart } from './base-chart';

declare var google: any;

export class ColumnChart extends BaseChart {
  constructor(type: string, chartComponent: any) {
    super(type, chartComponent);
  }

  createGoogleChart() {
    return new google.visualization.ColumnChart(this.chartComponent.chartDiv.nativeElement);
  }

}
