import { BaseChart } from './base-chart';

declare var google: any;

export class PieChart extends BaseChart {
  constructor(type: string, chartComponent: any) {
    super(type, chartComponent);
  }

  createGoogleChart() {
    return new google.visualization.PieChart(this.chartComponent.chartDiv.nativeElement);
  }

}
