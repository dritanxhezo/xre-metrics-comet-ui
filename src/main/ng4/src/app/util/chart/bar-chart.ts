import { BaseChart } from './base-chart';
import { ChartData } from '../../model/chart-data';
import { ChartColors } from '../../util/chart-colors';
import { LegendItem } from '../../model/legend-item';

declare var google: any;

export class BarChart extends BaseChart {
  showLegendHeader = false;
  chartHeightWidthRatio = 1.2;

  constructor(type: string, chartComponent: any) {
    super(type, chartComponent);
  }

  createGoogleChart() {
    return new google.visualization.BarChart(this.chartComponent.chartDiv.nativeElement);
  }

  getElementLegendLabel(event: any) {
    return this.dataView.getValue(event.row, 0);
  }

  findIndex(legendValue: string) {
    for (let i = 0; i < this.dataView.getNumberOfRows(); i++) {
      const value = this.dataView.getValue(i, 0);
      if (value === this.extractLegendValue(legendValue)) {
        return i;
      }
    }
    return null;
  }

  findLegendIndex(legendValue: string) {
    return this.chartComponent.legendItems.find(item => this.extractLegendValue(item.value) === legendValue).key;
  }

  getSelection(index: number): any {
    return [{row: index}];
  }

  generateDataTable (chartData: ChartData) {

     const dataTable = new google.visualization.arrayToDataTable([]);
     dataTable.addColumn('string');
     dataTable.addColumn('number');
     dataTable.addColumn({'type' : 'string', 'role' : 'style'});

     for (let i = 0; i < chartData.legend.length; i++) {
     let j = 0;
      Object.keys(chartData.data).forEach(key => {
         const arr = [];
          let value = chartData.data[key][i];
          switch (value) {
            case 'Infinity':
            case 'NaN': {
              value = null;
            }
          }
          arr.push(key);
          arr.push(value);
          arr.push('color:' + ChartColors[j++]);
          dataTable.addRow(arr);
      });
    }
   // dataTable.sort([{column:1, desc: true}])
    this.dataView = new google.visualization.DataView(dataTable);
  }

  setChartElements(lines: Array<number>) {
      lines.unshift(0);

      if (lines.length === 0) {
        console.log('not showing any lines;  ');
      }
      const actualLines = lines.map( function(value) {
          return value - 1;
      } );

      if (lines.length === 1) {
        lines.shift();
        this.dataView.setRows(lines);
      } else {
        this.dataView.setRows(actualLines.slice(1, actualLines.length - 2));
      }
  }

  sortComponentLegend(sortBy: string) {}

  createLegendEntries(chartData: ChartData) {
      const legendItems = new Array<LegendItem>();
      for (let i = 0; i < chartData.legend.length; i++) {
        let j = 0;
        Object.keys(chartData.data).forEach(key => {
          const value = chartData.data[key][i];
          const li = new LegendItem(j.toString(), key + ':' + value, true, ChartColors[j], 0, 0);
          legendItems.push(li);
          j++;
        });
      }
    return legendItems;
  }

  extractLegendValue(legendValue: string) {
    return legendValue.split(':')[0];
  }

}
