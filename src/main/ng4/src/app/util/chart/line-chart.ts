import { BaseChart } from './base-chart';

declare var google: any;

export class LineChart extends BaseChart {
  constructor(type: string, chartComponent: any) {
    super(type, chartComponent);
  }

  createGoogleChart() {
    return new google.visualization.LineChart(this.chartComponent.chartDiv.nativeElement);
  }

}
