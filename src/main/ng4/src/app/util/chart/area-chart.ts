import { BaseChart } from './base-chart';

declare var google: any;

export class AreaChart extends BaseChart {
  showMerge = false;

  constructor(type: string, chartComponent: any) {
    super(type, chartComponent);
  }

  createGoogleChart() {
    return new google.visualization.SteppedAreaChart(this.chartComponent.chartDiv.nativeElement);
  }

  selectElement(legendValue: string) {
    if (!this.chartComponent.mergeMode) {
      const index = this.findIndex(legendValue);
      if (index != null) {
        this.chartOptions.series[index - 1].areaOpacity = 0.8;
        this.reDrawChart();
        this.googleChart.setSelection(this.getSelection(index));
      }
    }
  }

  unselectElement(legendValue: string) {
    if (!this.chartComponent.mergeMode) {
      const index = this.findIndex(legendValue);
      if (index != null) {
        this.chartOptions.series[index - 1].areaOpacity = 0.3;
        this.reDrawChart();
        this.googleChart.setSelection([]);
      }
    }
  }

  getDatatableValue(value: any) {
    return value === null ? 0 : value;
  }

  updateChartSeriesElement(index: number) {
    this.chartSeries[index].lineWidth = 0.1;
    this.chartSeries[index].areaOpacity = 0.3;
  }
}
