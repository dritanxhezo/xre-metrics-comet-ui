import { PieChart } from './pie-chart';
import { AreaChart } from './area-chart';
import { ColumnChart } from './column-chart';
import { BarChart } from './bar-chart';
import { LineChart } from './line-chart';
import { BaseChart } from './base-chart';

export class ChartFactory {
    public static createChart(chartType: string, chartComponent: any): BaseChart {
    switch (chartType) {
        case 'PIE_CHART':    { return new PieChart(chartType, chartComponent); }
        case 'AREA_CHART':   { return new AreaChart(chartType, chartComponent); }
        case 'COLUMN_CHART': { return new ColumnChart(chartType, chartComponent); }
        case 'BAR_CHART':    { return new BarChart(chartType, chartComponent); }
        case 'LINE_CHART':   { return new LineChart(chartType, chartComponent); }
      }
    return new LineChart(chartType, chartComponent);
  }
}
