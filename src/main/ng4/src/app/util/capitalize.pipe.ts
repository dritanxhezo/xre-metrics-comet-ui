import { isString } from './util';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!isString(value)) { return null; };
    return value.split('_').map(s => (s.charAt(0).toUpperCase() + s.slice(1))).join(' ');
  }
}
