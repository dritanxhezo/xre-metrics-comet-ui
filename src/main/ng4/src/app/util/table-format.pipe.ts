import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'tableFormat'})
export class TableFormatPipe implements PipeTransform {
 transform(value: any, data: any, field: any, currentTable: any): any {
   const isHeartbeatLosesTable = currentTable.header === "heartbeat_loses";
   const isPerformanceViolationsTable = currentTable.header === "performance_violations";
   const idToSearch = isHeartbeatLosesTable ? "heartbeat_loses" : "performance_violations";

   if(isHeartbeatLosesTable || isPerformanceViolationsTable){

     const tableSchema = (d) => { return (d.id === idToSearch) }
     const filterColumn = data.find(tableSchema).tableDefinition.columns;

     const fieldSelector = (d) => { return d.sourceName === field }
     const filterLabel = filterColumn.find(fieldSelector);

     const format = filterLabel.format;

     if(format && format === "integer"){
       return value.replace(".0", "");
     } else if(format && format === "date"){
       const months = ["Jan", "Feb", "March", "April", "May", "Jun", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
       const temp = value.split("/");
       const month = months[parseInt(temp[1]) - 1];
       const day = temp[2];
       const year = temp[0];
       return month + ". " + day + ", " + year;
     }
   }

   return value
  }
}
