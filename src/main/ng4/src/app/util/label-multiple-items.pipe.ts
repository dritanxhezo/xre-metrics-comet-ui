import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'labelMultipleItems'})
export class LabelMultipleItemsPipe implements PipeTransform {
 transform(value: any, label: string): any {
    if (label && (label.length > 40)) {
      const arrayLength = label.split('||').length;
      return arrayLength + ' items selected';
    }
    return label;
  }
}
