import { Injectable } from '@angular/core';

@Injectable()
export class Constants {

  MIN1 = {
    MS: 60000,
    AVAILABLE_RANGE: 12 * 60,       // 12 hours
    SKIP: 10,                       // skip every 10 mins
    DEFAULT_RANGE: 60               // 1 hour, default range
  };

  MIN15 = {
    MS: 60000,
    AVAILABLE_RANGE: 7 * 24 * 60,   // a week
    SKIP: 60,                       // skip every hour
    DEFAULT_RANGE: 6 * 60           // six hours
  };

  MIN60 = {
    MS: 3600000,
    AVAILABLE_RANGE: 21 * 24 * 60, // three weeks
    SKIP: 7 * 60,                  // skip every 7 hours
    DEFAULT_RANGE: 42 * 60         // a day and a 3/4
  };

  MIN1440 = {
    MS: 86400000,
    AVAILABLE_RANGE: 30 * 24 * 60,  // a month
    SKIP: 1 * 24 * 60,              // skip every day
    DEFAULT_RANGE: 10 * 24 * 60     // 10 days
  };

  COOKIES_IS_AB_TEST_MODE = 'charts.isABTestMode';
  COOKIES_POPULATION = 'charts.population';
  COOKIES_JSESSIONID = 'JSESSIONID';

  isABTestMode = false;

  populations = {};

  selectedPopolations = undefined;

  AB_TESTING_SECURITY_ROLE = 'ROLE_ab-testing';
}
