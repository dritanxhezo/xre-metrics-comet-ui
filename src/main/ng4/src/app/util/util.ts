import { KeyValue } from '../model/key-value';
import { Setting } from '../model/setting';
import { Chart } from '../model/chart';
import { Constants } from './constants';
import { Permissions } from './permissions';

export const isArray = (value: any): boolean => Array.isArray(value);

export const isString = (value: any): boolean => typeof value === 'string';

export const isUndefined = (value: any): boolean => typeof value === 'undefined';

export function toArray(object: any): Array<any> {
  return isArray(object) ? object : Object.keys(object).map((key) => object[key]);
}

export function toArrayKeys(object: any): Array<any> {
  return isArray(object) ? object : Object.keys(object).map(key => key);
}

export function convertDateToUTC(date) {
    return new Date(date.getTime() + date.getTimezoneOffset() * 60000);
}

export function convertUTCDateToLocal(date) {
    return new Date(date.getTime() - date.getTimezoneOffset() * 60000);
}

export function convertDateToTimeStamp(date: Date): number {
  return date.getTime() + date.getTimezoneOffset();
}

export function formatDate(date: Date) {
  const minValue = date.getUTCMinutes();
  const lessThan10mins = (minValue < 10);
  if(lessThan10mins){
    return (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':0' + minValue
  } else {
    return (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes()
  }
}

export function convertDurationToTimeSpan(min: number): string {
  switch (min) {
    case 1: return 'HOUR';
    case 15: return 'DAY';
    case 60: return 'WEEK';
    case 1440: return 'MONTH';
    default: return '';
  }
}

export function convertTimeSpanToDuration(timeSpan: string): number {
  switch (timeSpan) {
    case 'HOUR': return 1;
    case 'DAY': return 15;
    case 'WEEK': return 60;
    case 'MONTH': return 1440;
    default: return 0;
  }
}

export function convertDurationForOldCharts(time: number): boolean {
  const mins = Math.floor(time / 60000);
  return mins > 60;
}

export function average(values: Array<number>): number {
  const sum = values.reduce(function(temp_sum, value){return temp_sum + value; }, 0);
  return sum / values.length;
}

export function standardDeviation(values: Array<number>): number {
  const avg = average(values);

  const squareDiffs = values.map(function(value) {
    const diff = value - avg;
    const sqrDiff = diff * diff;
    return sqrDiff;
  });

  const avgSquareDiff = average(squareDiffs);

  const stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

let modalCount = 0;

export function openModal() {
  if (modalCount === 0) {
    document.getElementById('wait-modal').style.display = 'block';
    document.getElementById('wait-fade').style.display = 'block';
  }
  modalCount++;
}

export function closeModal() {
  modalCount--;
  if (modalCount === 0) {
    document.getElementById('wait-modal').style.display = 'none';
    document.getElementById('wait-fade').style.display = 'none';
  }
}

export function randomColor() {
  const golden_ratio = 0.618033988749895;
  const h = (Math.random() + golden_ratio) % 1;
  return HSVtoRGB(h, 0.5, 0.95);
}

function HSVtoRGB(h, s, v) {
  let r, g, b, i, f, p, q, t;
  if (arguments.length === 1) {
      s = h.s, v = h.v, h = h.h;
  }
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
      case 0: r = v, g = t, b = p; break;
      case 1: r = q, g = v, b = p; break;
      case 2: r = p, g = v, b = t; break;
      case 3: r = p, g = q, b = v; break;
      case 4: r = t, g = p, b = v; break;
      case 5: r = v, g = p, b = q; break;
  }
  return '#' + toHex(Math.round(r * 255)) + toHex(Math.round(g * 255)) + toHex(Math.round(b * 255));
}

function toHex(n) {
 n = parseInt(n, 10);
 if (isNaN(n)) {return '00'; }
 n = Math.max(0, Math.min(n, 255));
 return '0123456789ABCDEF'.charAt((n - n % 16) / 16)
      + '0123456789ABCDEF'.charAt(n % 16);
}

  // Extract data sources support type:
  // 0 - no data source;
  // +1 - support regular data source;
  // +2 - support populations data source.
 export function getDataSourcesType(chart: Chart) {
    if (!chart.dataSources) {
      return 0;
    }
    let abTestingIndex = 0;
    let regularIndex = 0;
    for (const dataSource of chart.dataSources) {
      if (dataSource.populationsDataSource && dataSource.populationsDataSource.length > 0) {
        abTestingIndex = 2;
      }
      if (dataSource.dataSource && dataSource.dataSource.length > 0) {
        regularIndex = 1;
      }
    }
    return regularIndex + abTestingIndex;
  }


  export function updatePermission(userRoles: any, constants: Constants) {
    const permissions = new Permissions();
    if (!userRoles) {
      return permissions;
    }
    permissions.isABTestingAllowed = userRoles.findIndex(element => {
      return element.authority === constants.AB_TESTING_SECURITY_ROLE;
    }) > -1;
    return permissions;
  }
