import { isString } from './util';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'omitcolon'
})
export class OmitcolonPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!isString(value)) { return null; };
    return value.replace(':', '');
  }
}
