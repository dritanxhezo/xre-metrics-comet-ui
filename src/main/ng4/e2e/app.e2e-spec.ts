import { CometUiPage } from './app.po';

describe('comet-ui App', () => {
  let page: CometUiPage;

  beforeEach(() => {
    page = new CometUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
