/* ===========================================================================
* Copyright (c) 2017 Comcast Corp. All rights reserved.
* ===========================================================================
*
* Author: fnikon201
* Created: 10/10/2017
*/

package com.comcast.xremetrics.comet.config;

import com.comcast.xremetrics.comet.security.AclAuthentificationProvider;
import com.comcast.xremetrics.comet.controller.ProxyConfig;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
@EnableConfigurationProperties(ProxyConfig.class)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
    @Value("${authentication.enabled:true}")
    private boolean authenticationEnabled;


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        if (authenticationEnabled) {
            configureSecurity(http);
        } else {
            http
                    .csrf()
                    .disable();

            http.authorizeRequests()
                    .anyRequest()
                    .permitAll();
        }

    }

    private void configureSecurity(HttpSecurity http) throws Exception {
        http
        			.csrf()
                .disable();
        http.httpBasic().and()
		.authorizeRequests()
		.antMatchers("/security/**", "/**/populations/**")
		.authenticated();
        
		http
		.authorizeRequests()
		.anyRequest()
		.permitAll();
        
        http.logout().logoutUrl("/security/logout").deleteCookies("JSESSIONID").invalidateHttpSession(true);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(new AclAuthentificationProvider("comet"));
    }
}