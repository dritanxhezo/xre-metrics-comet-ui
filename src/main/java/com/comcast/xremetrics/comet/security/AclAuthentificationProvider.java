/* ===========================================================================
* Copyright (c) 2016 Comcast Corp. All rights reserved.
* ===========================================================================
*
* Author: Oleksii Shapovalov
* Created: 07/13/2016
*/

package com.comcast.xremetrics.comet.security;


import com.comcast.serviceaccess.dynamic.Auth;
import com.comcast.serviceaccess.model.AuthResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
public class AclAuthentificationProvider implements AuthenticationProvider {

    public static final String ROLE_PREFIX = "ROLE_";
    private final Auth authProvider;
    private final String serviceName;

    public AclAuthentificationProvider(String serviceName) {
        authProvider = new Auth();
        this.serviceName = serviceName;

    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String user = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();
        AuthResponse authResponse;
        try {
            authResponse = authProvider.attemptAuthentication(serviceName, user, password);
        } catch (com.comcast.serviceaccess.exception.AuthenticationException e) {
            log.warn("Authentification error", e);
            return null;
        }
        log.info("User logged in. username={} permissions={}", user, authResponse.getPermissions());
        Set<String> permissions = authResponse.getPermissions();
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String permission : permissions) {
            authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + permission));
        }
        return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), "", authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(UsernamePasswordAuthenticationToken.class);
    }
}
