package com.comcast.xremetrics.comet.controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
@EnableConfigurationProperties(ProxyConfig.class)
public class ProxyConfigController {
	
	@Autowired
	ProxyConfig proxyConfig;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllConfigs(@RequestParam(value="path", defaultValue="all") String path) {
    	if (path != null && !path.isEmpty() && path.equalsIgnoreCase("all")) {
    		Map result = proxyConfig.getPathMapper().entrySet().stream()
    														   .sorted(Map.Entry.comparingByKey())
    														   .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
    																   						(oldValue, newValue) -> oldValue, LinkedHashMap::new));
    		
    		return ResponseEntity.ok(result);
    	} else {
    		String pathUrl = proxyConfig.getPathMapper().get(path);
    		if (pathUrl != null && !pathUrl.isEmpty()) {
    			return ResponseEntity.ok(pathUrl);
    		}
    	}    	
    	return ResponseEntity.notFound().build();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity updateConfigs(@RequestBody Map pathMapping) {
    	if (pathMapping != null && pathMapping.size() > 0) {
    		proxyConfig.getPathMapper().putAll(pathMapping);
    		return ResponseEntity.ok().build();
    	} else {
    		return ResponseEntity.notFound().build();
    	}
    }
}
