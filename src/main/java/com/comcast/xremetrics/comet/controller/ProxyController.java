package com.comcast.xremetrics.comet.controller;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerMapping;

@RestController
@RequestMapping("/proxy")
@EnableConfigurationProperties(ProxyConfig.class)
public class ProxyController {
	
	@Autowired
	ProxyConfig proxyConfig;

    @Value("${base.url}")
    private String redirectBase;

    @Value("${app.version}")
    private String version;

    @Value("${app.builtTimeStamp}")
    private String builtTimeStamp;
	    
    @PostConstruct
    private void init() {
    }

    @RequestMapping(value = "**", method = {RequestMethod.POST, RequestMethod.PUT})
    public @ResponseBody Object proxyRequestsWithBody(HttpServletRequest request, @RequestHeader HttpHeaders headers, @RequestBody Object body) throws URISyntaxException {
    	System.out.printf("request=%s", request.getServletPath() + "~~~" + request.getQueryString());
    	Object response = proxyRequest(request, headers, body); 
    	System.out.printf("response=%s%n", response.toString());
        return response;
    }

    @RequestMapping(value = "**")
    public @ResponseBody Object proxyRequestsWithoutBody(HttpServletRequest request, @RequestHeader HttpHeaders headers) throws URISyntaxException {
    	System.out.printf("request=%s", request.getServletPath() + "~~~" + request.getQueryString());
    	Object response = proxyRequest(request, headers, null);
    	System.out.printf("response=%s%n", response.toString());
    	return response;
    }

    private @ResponseBody Object proxyRequest(HttpServletRequest request, @RequestHeader HttpHeaders headers, @RequestBody Object body) throws URISyntaxException {
        final RequestEntity<Object> requestEntity = convertToRequestEntity(request, headers, body);
        RestTemplate restTemplate = getRestTemplate(request);
        final ResponseEntity<Object> response = restTemplate.exchange(requestEntity, Object.class);
        return response;
    }

    private RestTemplate getRestTemplate(HttpServletRequest request) {
	    String resourcePath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
	    if (resourcePath != null && !resourcePath.isEmpty()){
	    	resourcePath = resourcePath.substring(resourcePath.indexOf("proxy/") + 6);
	    	while(resourcePath.startsWith("/")) {    // remove the extra slashes
	    		resourcePath = resourcePath.substring(1); 
	    	}
	    	String destUrl = getDestination(resourcePath);
	    	if (destUrl != null && !destUrl.isEmpty()) {
    			String[] destination = destUrl.split(":");
    			return restTemplate(destination[0], Integer.parseInt(destination[1]));
	    	}
	    }
		return restTemplate(null, 0);
	}
    
    private String getDestination(String resourcePath) {
    	String destination = redirectBase;
    	if (proxyConfig != null && proxyConfig.getPathMapper() != null && proxyConfig.getPathMapper().size() > 0) {
    		Optional<String> bestMatch = proxyConfig.getPathMapper().keySet().stream()
											.filter(key -> resourcePath.startsWith(key))
											.sorted((s1, s2) -> s2.length() - s1.length())
											.findFirst();
    		if (bestMatch.isPresent()) {
    			destination = proxyConfig.getPathMapper().get(bestMatch.get());
    		}
    	}
		return destination;
	}
    
    private RestTemplate restTemplate(String host, int port) {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();

        if (host != null && !host.isEmpty() && port>1) {
        	Proxy proxy= new Proxy(Type.HTTP, new InetSocketAddress(host, port));
        	requestFactory.setProxy(proxy);
        }

        return new RestTemplate(requestFactory);
    }
    
    private <T> RequestEntity<T> convertToRequestEntity(HttpServletRequest request, HttpHeaders headers, T body) throws URISyntaxException {
    	String uri = request.getRequestURI();
    	uri = uri.substring(uri.indexOf("proxy") + 6);
        final StringBuilder redirectUrl = new StringBuilder("http://").append(redirectBase).append("/").append(uri);
        final String queryString = request.getQueryString();
        if (queryString != null) {
            redirectUrl.append("?").append(queryString);
        }

        System.out.println("redirectUrl: " + redirectUrl.toString());
        final HttpMethod httpMethod = HttpMethod.valueOf(request.getMethod());
        return new RequestEntity<>(body, headers, httpMethod, new URI(redirectUrl.toString()));
    }
    
	@RequestMapping("/version")
    public String getVersion() throws URISyntaxException {
		return version + "; built on: " + builtTimeStamp;
    }    
}