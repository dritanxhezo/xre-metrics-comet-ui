package com.comcast.xremetrics.comet.controller;

import com.comcast.xremetrics.comet.config.SecurityConfig;
import com.comcast.xremetrics.model.UserRolesModel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.access.method.P;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/security")
@EnableConfigurationProperties(ProxyConfig.class)
public class SecurityController {

	@Value("${authentication.enabled:true}")
    private boolean authenticationEnabled;

    final String UNAUTHORIZED_USER_NAME = "unauth";
    final String AB_TESTING_SECURITY_ROLE = "ROLE_ab-testing";

    @RequestMapping("/user")
	public Principal user(Principal user) {
    		if (authenticationEnabled) {
    			return user;
    		} else {
    			return new Principal() {
    				public String getName() {
    					return "Undefined";
    				}
    				
    				public Map<String, String>[] getAuthorities() {
    					Map<String, String>[] authorities = new Map[1];
    					authorities[0] = new HashMap<>();
    					authorities[0].put("authority", AB_TESTING_SECURITY_ROLE);
    					return authorities;
    				}
    			};
    		}
	}
    
    @RequestMapping("/roles")
    public Collection getRoles(Authentication authentication) {
        List<String> roles = new ArrayList<>();
        if (authentication != null) {
        		for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
        			roles.add(grantedAuthority.getAuthority());
            }
        	} else if (!authenticationEnabled) {
        		roles.add(AB_TESTING_SECURITY_ROLE);
        	}
        return roles; 
    }

    @RequestMapping("/userandroles")
    public UserRolesModel getUserAndRoles(Authentication authentication) {
        String username = UNAUTHORIZED_USER_NAME;

        if (authentication != null) {
            username = String.valueOf(authentication.getPrincipal());
        }

        return new UserRolesModel(username, getRoles(authentication));
    }

}
