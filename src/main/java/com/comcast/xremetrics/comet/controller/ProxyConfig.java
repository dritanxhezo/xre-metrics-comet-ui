package com.comcast.xremetrics.comet.controller;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "proxy")
public class ProxyConfig {

	private Map<String, String> pathMapper;

    public Map<String, String> getPathMapper() {
        return pathMapper;
    }

    public void setPathMapper(Map<String, String> pathMapper) {
        this.pathMapper = pathMapper;
    }
}
