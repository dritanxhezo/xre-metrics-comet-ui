package com.comcast.xremetrics.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

/**
 * This model contains all required authentication information for and a list of his roles
 */
@Data
@AllArgsConstructor
public class UserRolesModel {
    private String user;
    private Collection roles;
}